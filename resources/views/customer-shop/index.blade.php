<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">
		<link rel="stylesheet" href="/vendor/css/style.css?v1">
	</head>
	<body class="bg5">
		<div class="shop-02">
			<div  class="banner-shop">
				<img src="/vendor/images/banner-shop.png">
				<div class="rec-center">
		          <img src="" class=" img-circle" >
		        </div>
			</div>


				<div class="detail">
					<div class="box-left">
						<img src="/vendor/images/icon-shop-01.png">
						<div class="res_name">{{ $shopCustomer->shop->shop_name }}</div>
					</div>
					<div class="box-center">

						<div class="cus_name">สวัสดี {{ $registerData->first_name.' '.$registerData->last_name }}</div>
					</div>
					<div class="box-right">
						<div class="text-qty"> ยอดสะสม</div>
						<div class="@if($shopCustomer->total >= 15) qty color-success @else qty @endif"> <!-- class="qty color-success" -->
							<div class="qty-cur">  {{ $shopCustomer->total }}  </div> <div>/   15 </div>
						</div>
					</div>

				</div>

				<div class="row">
					<div class="item">
						@foreach($datas as $keyRow => $dataRows)
							<ul class="m150-1">
								@foreach($dataRows as $dataColumn)
									@if($dataColumn == 1)
										<li> <img src="/vendor/images/m150-c.png" class="m150-c"></li>
									@else
										<li> <img src="/vendor/images/m150-g.png" class="m150-c"></li>
									@endif
								@endforeach
								@if($keyRow == 2)
									<li class="last-item">
										@if($shopCustomer->total >= 15)
											<img src="/vendor/images/bottle-m150_02.png">
										@else
											<img src="/vendor/images/bottle-m150_02BW.png">
										@endif
									</li>
								@endif
							</ul>
						@endforeach
					</div>
				</div>
				@if($shopCustomer->total >= 15)
					<a href="/by-pass-waiting-re/{{ $registerData->id }}/{{ $shopCustomer->shop_id }}"><img src="/vendor/images/btn-gift.png" class="btn-home"></a>
				@else
					<a href="/by-pass/{{ $registerData->id }}"><img src="/vendor/images/btn-home.png" class="btn-home"></a>
				@endif
				<!-- <a href=""><img src="images/btn-gift.png" class="btn-home"></a> -->
		</div>

	</body>
</html>