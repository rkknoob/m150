<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="/vendor/css/style.css?v1">
	</head>
	<body class="bg5">
		<div class="shop-02 shop-03">
			<div  class="banner-howto">
				<img src="/vendor/images/banner-shop3.png">
			</div>
			<div class="content">
				<table cellspacing="10">
					<tr>
						<th width="20%">ลำดับ</th>
						<th  width="60%">ชื่อลูกค้า</th>
						<th  width="20%">จำนวน / ขวด</th>
					</tr>
					@foreach($shopCustomers as $key => $shopCustomer)
						<tr>
							<td>{{ $key+1 }}</td>
							<td>{{ $shopCustomer->first_name." ".$shopCustomer->last_name }}</td>
							<td>{{ $shopCustomer->total_sum }}</td>
						</tr>
					@endforeach
				</table>
				<nav aria-label="Page navigation example " class="text-center">
				  <ul class="pagination pagination-lg">
				    {{ $shopCustomers->links() }}
				  </ul>
				</nav>

				<a href="/shop-check"><img src="/vendor/images/btn-home.png" class="btn-home"></a>
			</div>
		</div>

	</body>
</html>