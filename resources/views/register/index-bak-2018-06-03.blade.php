<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">
		<link rel="stylesheet" href="/vendor/css/style.css?v1">
		<style type="text/css">
			.help-block-bd,.help-block-tel,.help-block-name,.help-block-surname,.help-block-gender{
				display: none;
			}
		</style>
	</head>
	<body class="bg3">
		<div class="shop-02">

			<div  class="banner-shop">
				<img src="/vendor/images/banner-shop.png">
				<div class="rec-center">
		          <img src="{{ $lineUserProfile->avatar }}" class=" img-circle" >
		        </div>
			</div>

			<div class="text-register"> โปรดลงทะเบียนเพื่อเข้าร่วมกิจกรรม</div>
			<form  class="form-register" autocomplete="off" id="registerForm" method="post" action="{{ action('RegisterController@storeData') }}" onsubmit=" return validateForm();" autocomplete="off">
				{!! csrf_field() !!}
				<input type="hidden" name="line_user_id" value="{{ $lineUserProfile->id }}">
				<div class="col-sm-12 col-xs-12">
					<input type="text" name="first_name" id="name" class="input-register name" placeholder="ชื่อ">
					<div class="help-block-name help-block">กรุณากรอกชื่อ</div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<input type="text" name="last_name" id="surname" class="input-register surname" placeholder="นามสกุล">
					<div class="help-block-surname help-block">กรุณากรอกนามสกุล</div>
				</div>
				<div class="col-sm-7 col-xs-7" style="padding-right: 0">
					<div class="input-box bd" > ปีเกิด
					<ul>
						<li><input type="tel" name="year[]" class="input-bd" id="bd_n1" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext_bd(this)"> </li>
						<li><input type="tel" name="year[]" class="input-bd" id="bd_n2" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext_bd(this)"> </li>
						<li><input type="tel" name="year[]" class="input-bd " id="bd_n3" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext_bd(this)"> </li>
						<li><input type="tel" name="year[]" class="input-bd" id="bd_n4" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext_bd(this)"> </li>
					</ul>
					</div>
					<div class="help-block-bd help-block">กรุณากรอกปีเกิด 4 หลัก</div>
				</div>
				<div class="col-sm-5 col-xs-5">
					<div class="input-box sex">
						<span>เพศ</span>
						<ul style="    border-right: 1px solid;">
							<li><input type="checkbox" name="gender" id="gender-male" class="checkbox" value="ชาย"></li>
							<li>ชาย</li>
						</ul>
						<ul>
							<li><input type="checkbox" name="gender" id="gender-female" class="checkbox" value="หญิง"></li>
							<li>หญิง</li>
						</ul>

					</div>
					<div class="help-block-gender help-block">กรุณาเลือกเพศ</div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="input-box tel" > เบอร์โทรศัพท์
					<ul>
						<li><input type="tel" name="tel[]" class="input-tel" id="tel_n1" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)"> </li>
						<li><input type="tel" name="tel[]" class="input-tel" id="tel_n2" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)"> </li>
						<li ><input type="tel" name="tel[]" class="input-tel tel-s" id="tel_n3" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" > </li>

						<li><input type="tel" name="tel[]" class="input-tel" id="tel_n4" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)"> </li>
						<li><input type="tel" name="tel[]" class="input-tel" id="tel_n5" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)"> </li>
						<li ><input type="tel" name="tel[]" class="input-tel tel-s" id="tel_n6" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" > </li>

						<li><input type="tel" name="tel[]" class="input-tel" id="tel_n7" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)"> </li>
						<li><input type="tel" name="tel[]" class="input-tel" id="tel_n8" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)"> </li>
						<li><input type="tel" name="tel[]" class="input-tel" id="tel_n9" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)"> </li>
						<li><input type="tel" name="tel[]" class="input-tel" id="tel_n10" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)"> </li>
					</ul>
					</div>
					<div class="help-block-tel help-block">กรุณากรอกเบอร์โทรศัพท์ 10 หลัก</div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="box-condition" >
						<ul >
							<li  ><input type="checkbox" name="check-confirm" id="checkbox3" class=" checkbox checkbox-con" > 
							<span>ยอมรับเงื่อนไขและข้อตกลง</span>
							</li>
							<li  > <a href="/howto" class="read-con"> <img src="/vendor/images/icon-search.png"> อ่านเงื่อนไขและข้อตกลง </a> </li>
						</ul>
					</div>
					<div class="help-block-confirm help-block">กรุณากดยืนยันยอมรับเงื่อนไข</div>
				</div>

				<button class="btn-submit"  type="submit"><img src="/vendor/images/btn-regis.png" class="btn-regis"></button>
			</form>
		</div>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
		<script>

		$('.help-block-confirm').hide();

		$( "#gender-male" ).change(function() {
			var male = $('#gender-male:checkbox:checked').length;
			if(male == 1){
				$( "#gender-female" ).prop( "checked", false );
			}
		});
		$( "#gender-female" ).change(function() {
			var female = $('#gender-female:checkbox:checked').length;
			if(female == 1){
				$( "#gender-male" ).prop( "checked", false );
			}
		});

		function validateForm(){
			var length_year = 0;
			var length_tel = 0;
			var name = $('#name').val();
			var surname = $('#surname').val();
			var gender = $('input[name="gender"]:checked').length > 0;
			var checkConfirmation = $('input[name="check-confirm"]:checked').length;

			$('input[name^="year"]').each(function() {
			    if($(this).val() != ''){
			    	length_year += 1;
			    }
			});
			$('input[name^="tel"]').each(function() {
			    if($(this).val() != ''){
			    	length_tel += 1;
			    }
			});


			if(length_tel != 10){
				$('.help-block-tel').show();
			}

			if(length_year != 4){
				$('.help-block-bd').show();
			}
			if(name == ''){
				$('.help-block-name').show();
			}
			if(surname == ''){
				$('.help-block-surname').show();
			}

			if(gender == false){
				$('.help-block-gender').show();
			}

			if(checkConfirmation == false){
	            $('.help-block-confirm').show();
	        }

			if(length_tel != 10 || length_year != 4 || name == '' || surname == '' || gender == false || checkConfirmation == false){
				return false;
			}else{
				return true;
			}
		}

			document.querySelector('#name').onkeypress = CommonKeyPressIsAlpha;
			document.querySelector('#surname').onkeypress = CommonKeyPressIsAlpha;

			function CommonKeyPressIsAlpha(e){
			    e = e || event;
			    var keypressed = String.fromCharCode(e.keyCode || e.which);
			    var matched = (/^[ก-ฮะาิีึืุูโเแำไใ่้ั๊๋์a-z]+$/i).test (keypressed);
			   	if(matched == false){
			   		return false;

			   	}
			}

			$( "#name" ).keyup(function() {
			    var name = $('#name').val();
			  	if(name == ''){
					$('.help-block-name').show();
				}else{
					$('.help-block-name').hide();
				}
			});
			$( "#surname" ).keyup(function() {
			    var surname = $('#surname').val();
			    if(surname == ''){
					$('.help-block-surname').show();
				}else{
					$('.help-block-surname').hide();
				}
			});
			$( ".checkbox" ).click(function() {
				var gender = $('input[name="gender"]:checked').length > 0;
				if(gender == false){
					$('.help-block-gender').show();
				}else{
					$('.help-block-gender').hide();
				}
			});

			$( ".checkbox-con" ).click(function() {
				var checkConfirmation = $('input[name="check-confirm"]:checked').length > 0;
				if(checkConfirmation == false){
					$('.help-block-confirm').show();
				}else{
					$('.help-block-confirm').hide();
				}
			});

			function jumpNext(tel) {
				// alert(tel.id);
				if(tel.value != ''){
					if(tel.id == 'tel_n1'){
						$('#tel_n2').focus();
					}else if( tel.id == 'tel_n2' ){
						$('#tel_n3').focus();
					}else if( tel.id == 'tel_n3' ){
						$('#tel_n4').focus();
					}else if( tel.id == 'tel_n4' ){
						$('#tel_n5').focus();
					}else if( tel.id == 'tel_n5' ){
						$('#tel_n6').focus();
					}else if( tel.id == 'tel_n6' ){
						$('#tel_n7').focus();
					}else if( tel.id == 'tel_n7' ){
						$('#tel_n8').focus();
					}else if( tel.id == 'tel_n8' ){
						$('#tel_n9').focus();
					}else if( tel.id == 'tel_n9' ){
						$('#tel_n10').focus();
					}
				}else{
					if(tel.id == 'tel_n10'){
						$('#tel_n9').focus();
						var value = $("#tel_n9").val();
				        $("#tel_n9").val('');
				        $("#tel_n9").val(value);
					}else if( tel.id == 'tel_n9' ){
						$('#tel_n8').focus();
						var value = $("#tel_n8").val();
				        $("#tel_n8").val('');
				        $("#tel_n8").val(value);
					}else if( tel.id == 'tel_n8' ){
						$('#tel_n7').focus();
						var value = $("#tel_n7").val();
				        $("#tel_n7").val('');
				        $("#tel_n7").val(value);
					}else if( tel.id == 'tel_n7' ){
						$('#tel_n6').focus();
						var value = $("#tel_n6").val();
				        $("#tel_n6").val('');
				        $("#tel_n6").val(value);
					}else if( tel.id == 'tel_n6' ){
						$('#tel_n5').focus();
						var value = $("#tel_n5").val();
				        $("#tel_n5").val('');
				        $("#tel_n5").val(value);
					}else if( tel.id == 'tel_n5' ){
						$('#tel_n4').focus();
						var value = $("#tel_n4").val();
				        $("#tel_n4").val('');
				        $("#tel_n4").val(value);
					}else if( tel.id == 'tel_n4' ){
						$('#tel_n3').focus();
						var value = $("#tel_n3").val();
				        $("#tel_n3").val('');
				        $("#tel_n3").val(value);
					}else if( tel.id == 'tel_n3' ){
						$('#tel_n2').focus();
						var value = $("#tel_n2").val();
				        $("#tel_n2").val('');
				        $("#tel_n2").val(value);
					}else if( tel.id == 'tel_n2' ){
						$('#tel_n1').focus();
						 var value = $("#tel_n1").val();
				        $("#tel_n1").val('');
				        $("#tel_n1").val(value);
				    }
				}
				var length_tel = 0;
				$('input[name^="tel"]').each(function() {
				    if($(this).val() != ''){
				    	length_tel += 1;
				    }
				});
				if(length_tel == 10){
					$('.help-block-tel').hide();
				}else{
					$('.help-block-tel').show();
				}
			}

			function jumpNext_bd(bd) {
	
				if(bd.value != ''){
					if(bd.id == 'bd_n1'){
						$('#bd_n2').focus();
					}else if( bd.id == 'bd_n2' ){
						$('#bd_n3').focus();
					}else if( bd.id == 'bd_n3' ){
						$('#bd_n4').focus();
					}
				}else{
					if(bd.id == 'bd_n4'){
						$('#bd_n3').focus();
						var value = $("#bd_n3").val();
				        $("#bd_n3").val('');
				        $("#bd_n3").val(value);
					}else if( bd.id == 'bd_n3' ){
						$('#bd_n2').focus();
						var value = $("#bd_n2").val();
				        $("#bd_n2").val('');
				        $("#bd_n2").val(value);
					}else if( bd.id == 'bd_n2' ){
						$('#bd_n1').focus();
						var value = $("#bd_n1").val();
				        $("#bd_n1").val('');
				        $("#bd_n1").val(value);
					}
				}

				var length_year = 0;
				$('input[name^="year"]').each(function() {
				    if($(this).val() != ''){
				    	length_year += 1;
				    }
				});
				if(length_year == 4){
					$('.help-block-bd').hide();
				}else{
					$('.help-block-bd').show();
				}

			}
 		</script>
	</body>
</html>