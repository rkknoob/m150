<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">
		<link rel="stylesheet" href="/vendor/css/style.css">
    	<script src="/vendor/lib/device-uuid.js"></script>
	</head>
	<body class="bg5">
		<div class="shop-02">

			<div  class="banner-shop">
				<img src="/vendor/images/banner-shop.png">
				<div class="rec-center">
		          <img src="{{ $lineUserProfile->avatar }}" class=" img-circle" >
		        </div>
			</div>

			<div class="text-register"> โปรดลงทะเบียนเพื่อเข้าร่วมกิจกรรม</div>
			<form  class="form-register" id="action-form" action="{{ action('RegisterController@storeData') }}" method="post">
				{!! csrf_field() !!}
				<input type="hidden" id="imei" name="imei">
				<div class="col-sm-12 col-xs-12">
					<input type="text" name="first_name" id="first_name" class="input-register name" placeholder="ชื่อ" autocomplete="off">
				</div>
				<div class="col-sm-12 col-xs-12">
					<input type="text" name="last_name" id="last_name" class="input-register surname" placeholder="นามสกุล" autocomplete="off">
				</div>
				<div class="col-sm-7 col-xs-7" style="padding-right: 0">
					<div class="input-box bd" > ปีเกิด
					<ul>
						<li><input type="text" name="bd_n1" class="input-bd" id="bd_n1" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext_bd(this)" autocomplete="off"> </li>
						<li><input type="text" name="bd_n2" class="input-bd" id="bd_n2" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext_bd(this)" autocomplete="off"> </li>
						<li><input type="text" name="bd_n3" class="input-bd " id="bd_n3" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext_bd(this)" autocomplete="off"> </li>
						<li><input type="text" name="bd_n4" class="input-bd" id="bd_n4" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext_bd(this)" autocomplete="off"> </li>
					</ul>
					</div>
				</div>
				<div class="col-sm-5 col-xs-5">
					<div class="input-box sex">
						<span>เพศ</span>
						<ul style="    border-right: 1px solid;">
							<li><input type="checkbox" name="gender" id="gender-male" class="checkbox" value="ชาย"></li>
							<li>ชาย</li>
						</ul>
						<ul>
							<li><input type="checkbox" name="gender" id="gender-female" class="checkbox" value="หญิง"></li>
							<li>หญิง</li>
						</ul>
					</div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="input-box tel" > เบอร์โทรศัพท์
					<ul>
						<li><input type="text" name="tel_n1" class="input-tel" id="tel_n1" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" autocomplete="off"> </li>
						<li><input type="text" name="tel_n2" class="input-tel" id="tel_n2" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" autocomplete="off"> </li>
						<li ><input type="text" name="tel_n3" class="input-tel tel-s" id="tel_n3" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" autocomplete="off"> </li>

						<li><input type="text" name="tel_n4" class="input-tel" id="tel_n4" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" autocomplete="off"> </li>
						<li><input type="text" name="tel_n5" class="input-tel" id="tel_n5" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" autocomplete="off"> </li>
						<li ><input type="text" name="tel_n6" class="input-tel tel-s" id="tel_n6" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" autocomplete="off"> </li>

						<li><input type="text" name="tel_n7" class="input-tel" id="tel_n7" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" autocomplete="off"> </li>
						<li><input type="text" name="tel_n8" class="input-tel" id="tel_n8" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" autocomplete="off"> </li>
						<li><input type="text" name="tel_n9" class="input-tel" id="tel_n9" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" autocomplete="off"> </li>
						<li><input type="text" name="tel_n10" class="input-tel" id="tel_n10" maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)" autocomplete="off"> </li>
					</ul>
					</div>
				</div>
				<div class="col-sm-12 col-xs-12">
					<div class="box-condition" >
					<ul >
						<li  ><input type="checkbox" id="checkbox3" name="" class=" checkbox checkbox-con" > 
						<span>ยอมรับเงื่อนไขและข้อตกลง</span>
						</li>
						<li  > <a href="#" class="read-con"> <img src="/vendor/images/icon-search.png"> อ่านเงื่อนไขและข้อตกลง </a> </li>
					</ul>
					</div>
				</div>

				<a href="javascript:validate();"><img src="/vendor/images/btn-regis.png" class="btn-regis"></a>
			</form>
		</div>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
		<script type="text/javascript" src="/vendor/clientjs-master/dist/client.min.js"></script>
		<script>
			$(document).ready(function() {
				// var uuid = new DeviceUUID().get();
				// Create a new ClientJS object
				var client = new ClientJS();

				// Get the client's fingerprint id
				var fingerprint = client.getFingerprint();

				// Print the 32bit hash id to the console

				var du = new DeviceUUID().parse();
			    var dua = [
			        du.language,
			        du.os,
			        du.cpuCores,
			        du.isAuthoritative,
			        du.silkAccelerated,
			        du.isKindleFire,
			        du.isDesktop,
			        du.isMobile,
			        du.isTablet,
			        du.isWindows,
			        du.isLinux,
			        du.isLinux64,
			        du.isMac,
			        du.isiPad,
			        du.isiPhone,
			        du.isiPod,
			        du.isSmartTV,
			        du.pixelDepth,
			        du.isTouchScreen,
			        fingerprint
			    ];
			    var imei = du.hashMD5(dua.join('-'));
				$.ajax({
	          		url: "/check-imei?imei="+imei,
	          		success: function( result ) {
	          			if(result.is_check == 1){
	          				window.location.href = '/by-pass/'+result.regis_data_id;
	          			}
				    }
		        });
				$('#imei').val(imei);
			});
			$( "#gender-male" ).change(function() {
				var male = $('#gender-male:checkbox:checked').length;
				if(male == 1){
					$( "#gender-female" ).prop( "checked", false );
				}
			});
			$( "#gender-female" ).change(function() {
				var female = $('#gender-female:checkbox:checked').length;
				if(female == 1){
					$( "#gender-male" ).prop( "checked", false );
				}
			});

			function validate() {
		        var validFirstName = $('#first_name').val();
		        var validLastName = $('#last_name').val();
		        var male = $('#gender-male:checkbox:checked').length;
		        var female = $('#gender-female:checkbox:checked').length;
		        var validTel1 = $('#tel_n1').val();
		        var validTel2 = $('#tel_n2').val();
		        var validTel3 = $('#tel_n3').val();
		        var validTel4 = $('#tel_n4').val();
		        var validTel5 = $('#tel_n5').val();
		        var validTel6 = $('#tel_n6').val();
		        var validTel7 = $('#tel_n7').val();
		        var validTel8 = $('#tel_n8').val();
		        var validTel9 = $('#tel_n9').val();
		        var validTel10 = $('#tel_n10').val();
		        var validBirthYear1 = $('#bd_n1').val();
		        var validBirthYear2 = $('#bd_n2').val();
		        var validBirthYear3 = $('#bd_n3').val();
		        var validBirthYear4 = $('#bd_n4').val();
		        
		        var isSubmit = 1;
		        var msgError = ""; 
		        if(validFirstName == ""){
		            isSubmit = 0;
		            msgError += "กรุณากรอก ชื่อ \n";
		        }
		        if(validLastName == ""){
		            isSubmit = 0;
		            msgError += "กรุณากรอก นามสกุล \n";
		        }
		        if(male == 0 && female == 0){
		            isSubmit = 0;
		            msgError += "กรุณาเลือก เพศ \n";
		        }
		        
		        if(validTel1 == "" || validTel2 == "" || validTel3 == "" || validTel4 == "" || validTel5 == "" || validTel5 == "" || validTel6 == "" || validTel7 == "" || validTel8 == "" || validTel9 == "" || validTel10 == ""){
		            isSubmit = 0;
		            msgError += "กรุณากรอก เบอร์โทรศัพท์ของท่านเป็นเลข 10 หลัก \n";
		        }

		        if(validBirthYear1 == "" || validBirthYear2 == "" || validBirthYear3 == "" || validBirthYear4 == ""){
		            isSubmit = 0;
		            msgError += "กรุณากรอก ปีเกิด\n";
		        }


		        if(document.getElementById("checkbox3").checked == false){
		            isSubmit = 0;
		            msgError += "กรุณายืนยันการสมัครและรับรองเงื่อนไข \n";
		        }

		        if(isSubmit){
		            $('#action-form').submit();
		            // document.getElementById("action-form").submit();
		        }else{
		            alert(msgError);
		        }
		      }

			function generateUUID() {
			    var d = new Date().getTime();
			    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			        var r = (d + Math.random()*16)%16 | 0;
			        d = Math.floor(d/16);
			        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
			    });
			    return uuid;
			};

			function jumpNext(tel) {
				// alert(tel.id);
				if(tel.value != ''){
					if(tel.id == 'tel_n1'){
						$('#tel_n2').focus();
					}else if( tel.id == 'tel_n2' ){
						$('#tel_n3').focus();
					}else if( tel.id == 'tel_n3' ){
						$('#tel_n4').focus();
					}else if( tel.id == 'tel_n4' ){
						$('#tel_n5').focus();
					}else if( tel.id == 'tel_n5' ){
						$('#tel_n6').focus();
					}else if( tel.id == 'tel_n6' ){
						$('#tel_n7').focus();
					}else if( tel.id == 'tel_n7' ){
						$('#tel_n8').focus();
					}else if( tel.id == 'tel_n8' ){
						$('#tel_n9').focus();
					}else if( tel.id == 'tel_n9' ){
						$('#tel_n10').focus();
					}
				}else{
					if(tel.id == 'tel_n10'){
						$('#tel_n9').focus();
						var value = $("#tel_n9").val();
				        $("#tel_n9").val('');
				        $("#tel_n9").val(value);
					}else if( tel.id == 'tel_n9' ){
						$('#tel_n8').focus();
						var value = $("#tel_n8").val();
				        $("#tel_n8").val('');
				        $("#tel_n8").val(value);
					}else if( tel.id == 'tel_n8' ){
						$('#tel_n7').focus();
						var value = $("#tel_n7").val();
				        $("#tel_n7").val('');
				        $("#tel_n7").val(value);
					}else if( tel.id == 'tel_n7' ){
						$('#tel_n6').focus();
						var value = $("#tel_n6").val();
				        $("#tel_n6").val('');
				        $("#tel_n6").val(value);
					}else if( tel.id == 'tel_n6' ){
						$('#tel_n5').focus();
						var value = $("#tel_n5").val();
				        $("#tel_n5").val('');
				        $("#tel_n5").val(value);
					}else if( tel.id == 'tel_n5' ){
						$('#tel_n4').focus();
						var value = $("#tel_n4").val();
				        $("#tel_n4").val('');
				        $("#tel_n4").val(value);
					}else if( tel.id == 'tel_n4' ){
						$('#tel_n3').focus();
						var value = $("#tel_n3").val();
				        $("#tel_n3").val('');
				        $("#tel_n3").val(value);
					}else if( tel.id == 'tel_n3' ){
						$('#tel_n2').focus();
						var value = $("#tel_n2").val();
				        $("#tel_n2").val('');
				        $("#tel_n2").val(value);
					}else if( tel.id == 'tel_n2' ){
						$('#tel_n1').focus();
						 var value = $("#tel_n1").val();
				        $("#tel_n1").val('');
				        $("#tel_n1").val(value);
				    }
				}
			}

			function jumpNext_bd(bd) {
	
				if(bd.value != ''){
					if(bd.id == 'bd_n1'){
						$('#bd_n2').focus();
					}else if( bd.id == 'bd_n2' ){
						$('#bd_n3').focus();
					}else if( bd.id == 'bd_n3' ){
						$('#bd_n4').focus();
					}
				}else{
					if(bd.id == 'bd_n4'){
						$('#bd_n3').focus();
						var value = $("#bd_n3").val();
				        $("#bd_n3").val('');
				        $("#bd_n3").val(value);
					}else if( bd.id == 'bd_n3' ){
						$('#bd_n2').focus();
						var value = $("#bd_n2").val();
				        $("#bd_n2").val('');
				        $("#bd_n2").val(value);
					}else if( bd.id == 'bd_n2' ){
						$('#bd_n1').focus();
						var value = $("#bd_n1").val();
				        $("#bd_n1").val('');
				        $("#bd_n1").val(value);
					}
				}
			}
 		</script>
	</body>
</html>