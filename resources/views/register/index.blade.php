<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>M150</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">
	<link rel="stylesheet" href="/vendor/css/style.css?v=1.1">
	<style type="text/css">
		.help-block-bd,.help-block-tel,.help-block-name,.help-block-surname,.help-block-gender{
			display: none;
		}
	</style>
</head>
<body class="bg5">
<div class="shop-02">

	<div  class="banner-shop">
		<img src="/vendor/images/banner-shop.png">
		<div class="rec-center">
			<img src="{{ $lineUserProfile->avatar }}" class=" img-circle" >
		</div>
	</div>

	<div class="text-register"> โปรดลงทะเบียนเพื่อเข้าร่วมกิจกรรม</div>
	<form  class="form-register" autocomplete="off" id="registerForm" method="post" action="{{ action('RegisterController@storeData') }}" onsubmit=" return validateForm();">
		{!! csrf_field() !!}
		<input type="hidden" name="line_user_id" value="{{ $lineUserProfile->id }}">
		<div class="col-sm-12 col-xs-12">
			<input type="text" name="first_name" id="name" class="input-register name" placeholder="ชื่อ">
			<div class="help-block-name help-block">กรุณากรอกชื่อ</div>
		</div>
		<div class="col-sm-12 col-xs-12">
			<input type="text" name="last_name" id="surname" class="input-register surname" placeholder="นามสกุล" >
			<div class="help-block-surname help-block">กรุณากรอกนามสกุล</div>
		</div>
		<div class="col-sm-7 col-xs-7" style="padding-right: 0">
			<input type="tel" name="year_birth" id="year" class="input-register bd" placeholder="ปี พ.ศ เกิด (2499)" maxlength="4" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext_bd(this)">
			<div class="help-block-bd help-block">กรุณากรอกปี พ.ศ เกิด 4 หลัก</div>
		</div>
		<div class="col-sm-5 col-xs-5">
			<div class="input-box sex">
				<span>เพศ</span>
				<ul style="    border-right: 1px solid;">
					<li><input type="checkbox" name="gender" value="ชาย" class="checkbox" id="gender-male"></li>
					<li>ชาย</li>
				</ul>
				<ul>
					<li><input type="checkbox" name="gender" value="หญิง" class="checkbox" id="gender-female"></li>
					<li>หญิง</li>
				</ul>

			</div>
			<div class="help-block-gender help-block">กรุณาเลือกเพศ</div>
		</div>
		<div class="col-sm-12 col-xs-12">
			<input type="tel" name="phone_number" id="tel" class="input-register tel" placeholder="เบอร์โทรศัพท์ 10 หลัก (081XXXXXXX)" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' onkeyup="jumpNext(this)">
			<div class="help-block-tel help-block">กรุณากรอกเบอร์โทรศัพท์ 10 หลัก (081XXXXXXX)</div>
		</div>
		<div class="col-sm-12 col-xs-12">
			<div class="box-condition" >
				<ul >
					<li  ><input type="checkbox" name="check-confirm" class=" checkbox checkbox-con" id="checkbox3">
						<span><a href="/term-condition" class="read-con">ยอมรับเงื่อนไขและข้อตกลง</a></span>
					</li>
					<li  > <a href="/howto" class="read-con"> <img src="/vendor/images/icon-search.png"> อ่านเงื่อนไขและข้อตกลง </a> </li>
				</ul>
			</div>
			<div class="help-block-confirm help-block">กรุณากดยืนยันยอมรับเงื่อนไข</div>
		</div>

		<button class="btn-submit"  type="submit "><img src="/vendor/images/btn-regis.png" class="btn-regis"></a></button>
	</form>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
<script>
    $('.help-block-confirm').hide();

    $( "#gender-male" ).change(function() {
        var male = $('#gender-male:checkbox:checked').length;
        if(male == 1){
            $( "#gender-female" ).prop( "checked", false );
        }
    });
    $( "#gender-female" ).change(function() {
        var female = $('#gender-female:checkbox:checked').length;
        if(female == 1){
            $( "#gender-male" ).prop( "checked", false );
        }
    });

    function validateForm(){
        var length_year = 0;
        var name = $('#name').val();
        var surname = $('#surname').val();
        var gender = $('input[name="gender"]:checked').length > 0;
        var checkConfirmation = $('input[name="check-confirm"]:checked').length;

        var length_tel = $('#tel').val().length;
        var length_year = $('#year').val().length;

        if(length_tel != 10){
            $('.help-block-tel').show();
        }

        if(length_year != 4){
            $('.help-block-bd').show();
        }

        // $('input[name^="year"]').each(function() {
        //     if($(this).val() != ''){
        //     	length_year += 1;
        //     }
        // });
        // $('input[name^="tel"]').each(function() {
        //     if($(this).val() != ''){
        //     	length_tel += 1;
        //     }
        // });


        if(name == ''){
            $('.help-block-name').show();
        }
        if(surname == ''){
            $('.help-block-surname').show();
        }

        if(gender == false){
            $('.help-block-gender').show();
        }

        if(checkConfirmation == false){
            $('.help-block-confirm').show();
        }

        if(length_tel != 10 || length_year != 4 || name == '' || surname == '' || gender == false || checkConfirmation == false){
            return false;
        }else{
            return true;
        }
    }

    document.querySelector('#name').onkeypress = CommonKeyPressIsAlpha;
    document.querySelector('#surname').onkeypress = CommonKeyPressIsAlpha;

    function CommonKeyPressIsAlpha(e){
        e = e || event;
        var keypressed = String.fromCharCode(e.keyCode || e.which);
        var matched = (/^[ก-ฮะาิีึืุูโเแำไใ่้ั๊๋์a-z]+$/i).test (keypressed);
        if(matched == false){
            return false;

        }
    }

    $( "#name" ).keyup(function() {
        var name = $('#name').val();
        if(name == ''){
            $('.help-block-name').show();
        }else{
            $('.help-block-name').hide();
        }
    });
    $( "#surname" ).keyup(function() {
        var surname = $('#surname').val();
        if(surname == ''){
            $('.help-block-surname').show();
        }else{
            $('.help-block-surname').hide();
        }
    });
    $( ".checkbox" ).click(function() {
        var gender = $('input[name="gender"]:checked').length > 0;
        if(gender == false){
            $('.help-block-gender').show();
        }else{
            $('.help-block-gender').hide();
        }
    });

    $( ".checkbox-con" ).click(function() {
        var checkConfirmation = $('input[name="check-confirm"]:checked').length > 0;
        if(checkConfirmation == false){
            $('.help-block-confirm').show();
        }else{
            $('.help-block-confirm').hide();
        }
    });

    function jumpNext(tel) {

        var length_tel = $('#tel').val().length;
        if(length_tel == 10){
            $('.help-block-tel').hide();
        }else{
            $('.help-block-tel').show();
        }
    }

    function jumpNext_bd(bd) {

        var length_year = $('#year').val().length;
        if(length_year == 4){
            $('.help-block-bd').hide();
        }else{
            $('.help-block-bd').show();
        }

    }
</script>
</body>
</html>