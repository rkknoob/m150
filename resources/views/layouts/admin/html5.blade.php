<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="UTF-8">
	    <title>Yellow Cloud Platform</title>
	    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	    <![endif]-->
	    <!-- global css -->
	    <link href="/admin/css/app.css" rel="stylesheet" type="text/css" />
	    <!-- end of global css -->
	    <!--page level css -->
	    <link href="/admin/vendors/fullcalendar/css/fullcalendar.css" rel="stylesheet" type="text/css" />
	    <link href="/admin/css/pages/calendar_custom.css" rel="stylesheet" type="text/css" />
	    <link rel="stylesheet" media="all" href="/admin/vendors/bower-jvectormap/css/jquery-jvectormap-1.2.2.css" />
	    <link rel="stylesheet" href="/admin/vendors/animate/animate.min.css">
	    <link href="/admin/vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css" />
	    <link rel="stylesheet" type="text/css" href="/admin/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css">
	    <link rel="stylesheet" href="/admin/css/pages/only_dashboard.css" />
	    <link rel="stylesheet" href="/admin/css/pages/jscharts.css" />
	    <link type="text/css" rel="stylesheet" href="/admin/css/pages/flot.css" />
	    <!--end of page level css-->
	    <!--page level css -->
	    <link href="/admin/vendors/c3/c3.min.css" rel="stylesheet" type="text/css" />
	    <link href="/admin/vendors/morrisjs/morris.css" rel="stylesheet" type="text/css" />
	    <link href="/admin/css/pages/piecharts.css" rel="stylesheet" type="text/css" />
    	<link href="/admin/vendors/clockface/css/clockface.css" rel="stylesheet" type="text/css" />
    	<link href="/admin/vendors/jasny-bootstrap/css/jasny-bootstrap.css" rel="stylesheet" type="text/css" />
	    <!--end of page level css-->
		@yield('head')
	</head>
	<body class="skin-josh">
		<header class="header">
			@include('layouts.admin.header')
		</header>
		<div class="wrapper row-offcanvas row-offcanvas-left">
			@include('layouts.admin.left')
			@yield('body')
		</div>
		<footer>
			@include('layouts.admin.footer')
		</footer>
		<!-- ./wrapper -->
	    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
	        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
	    </a>
	    <!-- global js -->
	    <script src="/admin/js/app.js" type="text/javascript"></script>
	    <!-- end of global js -->
	    <!-- begining of page level js -->
	    <!-- EASY PIE CHART JS -->
	    <script src="/admin/vendors/jquery.easy-pie-chart/js/easypiechart.min.js"></script>
	    <script src="/admin/vendors/jquery.easy-pie-chart/js/jquery.easypiechart.min.js"></script>
	    <script src="/admin/js/jquery.easingpie.js"></script>
	    <!--end easy pie chart -->
	    <!--for calendar-->
	    <script src="/admin/vendors/moment/js/moment.min.js" type="text/javascript"></script>
	    <script src="/admin/vendors/fullcalendar/js/fullcalendar.min.js" type="text/javascript"></script>
	    <!--   Realtime Server Load  -->
	    <script src="/admin/vendors/flotchart/js/jquery.flot.js" type="text/javascript"></script>
	    <script src="/admin/vendors/flotchart/js/jquery.flot.resize.js" type="text/javascript"></script>
	    <!--Sparkline Chart-->
	    <script src="/admin/vendors/sparklinecharts/jquery.sparkline.js"></script>

	    <script type="text/javascript" src="/admin/vendors/countUp.js/js/countUp.js"></script>
	    <!--   maps -->
	    <script src="/admin/vendors/bower-jvectormap/js/jquery-jvectormap-1.2.2.min.js"></script>
	    <script src="/admin/vendors/bower-jvectormap/js/jquery-jvectormap-world-mill-en.js"></script>
	    <script src="/admin/vendors/chartjs/Chart.js"></script>
	    <script src="/admin/vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
	    <script type="text/javascript" src="/admin/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	    <script src="/admin/js/pages/chartjs.js" type="text/javascript"></script>
	    <script src="/admin/js/pages/dashboard.js" type="text/javascript"></script>
	    <script src="/admin/vendors/chartjs/Chart.js" type="text/javascript"></script>
	    <!-- end of page level js -->

	    <script language="javascript" type="text/javascript" src="/admin/vendors/flotchart/js/jquery.flot.js"></script>
	    <script language="javascript" type="text/javascript" src="/admin/vendors/flotchart/js/jquery.flot.stack.js"></script>
	    <script language="javascript" type="text/javascript" src="/admin/vendors/flotchart/js/jquery.flot.crosshair.js"></script>
	    <script language="javascript" type="text/javascript" src="/admin/vendors/flotchart/js/jquery.flot.time.js"></script>
	    <script language="javascript" type="text/javascript" src="/admin/vendors/flotchart/js/jquery.flot.selection.js"></script>
	    <script language="javascript" type="text/javascript" src="/admin/vendors/flotchart/js/jquery.flot.symbol.js"></script>
	    <script language="javascript" type="text/javascript" src="/admin/vendors/flotchart/js/jquery.flot.resize.js"></script>
	    <script language="javascript" type="text/javascript" src="/admin/vendors/flotchart/js/jquery.flot.categories.js"></script>
	    <script language="javascript" type="text/javascript" src="/admin/vendors/splinecharts/jquery.flot.spline.js"></script>
	    <script language="javascript" type="text/javascript" src="/admin/vendors/flot.tooltip/js/jquery.flot.tooltip.js"></script>
	    <script language="javascript" type="text/javascript" src="/admin/js/pages/customcharts.js"></script>

	    <!-- begining of page level js -->
	    <script type="text/javascript" src="/admin/vendors/flotchart/js/jquery.flot.pie.js"></script>
	    <script type="text/javascript" src="/admin/vendors/d3/d3.min.js"></script>
	    <script type="text/javascript" src="/admin/vendors/d3pie/d3pie.min.js"></script>
	    <script type="text/javascript" src="/admin/vendors/c3/c3.min.js"></script>
	    <script type="text/javascript" src="/admin/vendors/morrisjs/morris.min.js"></script>
	    <script type="text/javascript" src="/admin/js/pages/custompiecharts.js"></script>
	    <!-- end of page level js -->

	    <script src="/admin/vendors/clockface/js/clockface.js" type="text/javascript"></script>
	    <script src="/admin/vendors/jasny-bootstrap/js/jasny-bootstrap.js" type="text/javascript"></script>
	    <script src="/admin/js/pages/datepicker.js" type="text/javascript"></script>
	</body>
</html>