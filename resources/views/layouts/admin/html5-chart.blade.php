<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="UTF-8">
	    <title>Yellow Cloud Platform</title>
	    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	    <!-- global css -->
	    <!-- bootstrap 3.2.0 -->
	    <link href="/admin/css/app.css" rel="stylesheet" type="text/css" />
	    <!-- end of global css -->
	    <!--page level css -->
	    <link rel="stylesheet" href="/admin/css/pages/jscharts.css" />
	    <!--end of page level css-->
		@yield('head')
	</head>
	<body class="skin-josh">
		<header class="header">
			@include('layouts.admin.header')
		</header>
		<div class="wrapper row-offcanvas row-offcanvas-left">
			@include('layouts.admin.left')
			@yield('body')
		</div>
		<footer>
			@include('layouts.admin.footer')
		</footer>
		<!-- ./wrapper -->
	    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
	        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
	    </a>
	    <!-- global js -->
	    <script src="/admin/js/app.js" type="text/javascript"></script>
	    <!-- end of global js -->
	    <!-- begining of page level js -->
	    <script src="/admin/vendors/chartjs/Chart.js" type="text/javascript"></script>
	    <script src="/admin/js/pages/chartjs.js" type="text/javascript"></script>
	    <!-- end of page level js -->
	</body>
</html>