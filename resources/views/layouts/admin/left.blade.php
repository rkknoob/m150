<aside class="left-side sidebar-offcanvas">
    <section class="sidebar ">
        <div class="page-sidebar  sidebar-nav">
            <div class="clearfix"></div>
            <!-- BEGIN SIDEBAR MENU -->
            <ul id="menu" class="page-sidebar-menu">
                <li class="@if($page == 'home') active @endif">
                    <a href="/admin-home">
                        <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                        <span class="title">Home</span>
                    </a>
                </li>
                <li class="@if($page == 'register-detail') active @endif">
                    <a href="/admin-register-detail">
                        <i class="livicon" data-name="register-detail" data-c="#5bc0de" data-hc="#5bc0de" data-size="18" data-loop="true"></i>
                        <span class="title">ข้อมูลลงทะเบียน</span>
                    </a>
                </li>
                <!-- <li class="@if($page == 'reedeem-detail') active @endif">
                    <a href="/admin-reedeem-detail">
                        <i class="livicon" data-name="users" data-c="#418BCA" data-hc="#418BCA" data-size="18" data-loop="true"></i>
                        <span class="title">ผู้บริโภคที่มา Redeem</span>
                    </a>
                </li> -->
                <li class="@if($page == 'customer-return') active @endif">
                    <a href="/admin-customer-return">
                        <i class="livicon" data-name="barchart" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">ผู้บริโภค ที่มีการกลับมาซื้อซ้ำ</span>
                    </a>
                </li>
                <li class="@if($page == 'shop-accumulated') active @endif">
                    <a href="/admin-shop-accumulated">
                        <i class="livicon" data-name="shopping-cart" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">ร้านค้าที่มียอดสะสม</span>
                    </a>
                </li>
                <li class="@if($page == 'customer-accumulated') active @endif">
                    <a href="/admin-customer-accumulated">
                        <i class="livicon" data-name="shopping-cart" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">ลูกค้าที่มียอดสะสม</span>
                    </a>
                </li>
                <li class="@if($page == 'shop-reedeem') active @endif">
                    <a href="/admin-shop-reedeem">
                        <i class="livicon" data-name="shopping-cart" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">ร้านค้าที่ถูก Redeem</span>
                    </a>
                </li>
                <li class="@if($page == 'shop-detail') active @endif">
                    <a href="/admin-shop-detail">
                        <i class="livicon" data-name="shopping-cart" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">ข้อมูลร้านค้า</span>
                    </a>
                </li>
                <!-- <li>
                    <a href="#">
                        <i class="livicon" data-name="download" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">Download File</span>
                    </a>
                </li> -->
                <li class="@if($page == 'upload-data') active @endif">
                    <a href="/admin-upload">
                        <i class="livicon" data-name="upload" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">Upload Files</span>
                    </a>
                </li>
                <li class="@if($page == 'upload-status') active @endif">
                    <a href="/admin-upload-status">
                        <i class="livicon" data-name="upload" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">Upload Status</span>
                    </a>
                </li>
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    </section>
    <!-- /.sidebar -->
</aside>