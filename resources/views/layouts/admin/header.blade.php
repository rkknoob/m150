

<?php
    $user = \App\LoginData\User::find(1);
?>
<a href="/admin-home" class="logo">
    <img width="150px" height="40px" src="/img/yellow_logo.png" alt="Logo">
</a>
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <div>
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <div class="responsive_nav"></div>
        </a>
    </div>
    <div class="navbar-right">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="/img/yellow_logo_mini.png" width="35" class="img-circle img-responsive pull-left" height="35" alt="riot"> <!-- Yellow Idea Main Logo -->
                    <div class="riot">
                        <div>
                            {{$user->email}} <!-- Login Name -->
                            <span>
                                <i class="caret"></i>
                            </span>
                        </div>
                    </div>
                </a>
                <ul class="dropdown-menu">
                    <!-- Menu Body -->
                    <li role="presentation"></li>
                    <li>
                        <a href="/admin-logout">
                            <i class="livicon" data-name="sign-out" data-s="18"></i> Logout
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>