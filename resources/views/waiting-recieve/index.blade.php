<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">
		<link rel="stylesheet" href="/vendor/css/style.css?v1">
	</head>
	<body class="bg5">
		<input type="hidden" name="register_id" id="register_id" value="{{ $registerData->id }}">
		<input type="hidden" name="shop_id" id="shop_id" value="{{ $shopCustomer->shop_id }}">
		<div class="shop-02">
			<div  class="banner-shop">
				<img src="/vendor/images/banner-shop.png">
				<div class="rec-center">
		          <img src="" class=" img-circle" >
		        </div>
			</div>

			<div class="cus_name_p9">{{ $registerData->first_name.' '.$registerData->last_name }}</div>
			<div class="box">
				<div>กรุณาอย่ากดออกจากรายการ</div>
				<div>ระบบกำลังนำท่านสู่หน้ารับของรางวัล</div>
				<img src="/vendor/images/Pack_FRONT_New_Small_BW2.png" class="">
				<div class="per_load" id="per_load">0%</div>
				<div>กำลังโหลด</div>
			</div>
		</div>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				var registerId = $('#register_id').val();
				var shopId = $('#shop_id').val();
				$('#per_load').empty();
	          	$('#per_load').append("15%");
				$.ajax({
	          		url: "/customer-shop-recieve?register_id="+registerId+"&shop_id="+shopId,
	          		success: function( result ) {
	          			setTimeout(function() {
	          				$('#per_load').empty();
						  	$('#per_load').append("30%");
						}, 1000);
						setTimeout(function() {
							$('#per_load').empty();
						  	$('#per_load').append("45%");
						}, 2000);
						setTimeout(function() {
							$('#per_load').empty();
						 	$('#per_load').append("70%");
						}, 3000);
						setTimeout(function() {
							$('#per_load').empty();
						  	$('#per_load').append("100%");
						}, 4000);
						setTimeout(function() {
						  window.location.href = '/by-pass-recieve/'+registerId+'/'+shopId;
						}, 5000);
				    }
		        });
			});
		</script>
	</body>
</html>