<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">
		<link rel="stylesheet" href="/vendor/css/style.css?v1">
	</head>
	<body class="bg5">
		<div class="shop-02">
			<div  class="banner-shop">
				<img src="/vendor/images/banner-shop.png">
				<div class="rec-center">
		          <img src="{{ $lineUserProfile->avatar }}" class=" img-circle" >
		        </div>
			</div>
			<input type="hidden" name="register_id" id="register_id" value="{{$registerId}}">
			<input type="hidden" name="shop_id" id="shop_id" value="">
			<input type="hidden" name="qty" id="qty" value="0">
			<input type="hidden" name="customer_qty" id="customer_qty" value="">

			<div class="cus_name_p9">{{ $registerData->first_name.' '.$registerData->last_name }}</div>

				<div class="shop-code">
					<div class="text-shop-code"> <img src="/vendor/images/icon-shop-01.png"> <label> สำหรับร้านค้ากรอกรหัสเท่านั้น</label> </div>
					<input type="text" name="shop_code" id="shop_code" class="shop-code-input" autocomplete="off">
					<div class="text-center" style="margin-top: 20px"><a href="javascript:checkShop();" class="btn-confirm"><img src="/vendor/images/btn-confirm.png"></a></div>
				</div>

			<div class="shop-gift">
				<div class="text-shop-gift"> <img src="/vendor/images/gift.png"> <label> ยอดซื้อสะสมจากร้านค้าของท่าน</label> </div>
				@if($shopCustomers->count() > 0)
					@foreach($shopCustomers as $shopCustomer)
						<div class="col-sm-6 col-xs-6">
							<a href="/by-pass-customer-code/{{$registerData->id}}/{{$shopCustomer->shop_id}}">
								<div class="box-shop-qty">
									<div class="shop_name"> {{ $shopCustomer->shop->shop_name }} </div>
									<div class="shop-qty ">
										<div class="shop-qty-cur red">  {{ $shopCustomer->total }}  </div> / <div class="qty-all">  15 </div>
									</div>
								</div>
							</a>
						</div>
					@endForeach
				@endif
				<!-- <div class="col-sm-6 col-xs-6">
					<div class="box-shop-qty">
						<div class="shop_name"> ร้านเฮียยุทธ์</div>
						<div class="shop-qty ">
							<div class="shop-qty-cur red">  2  </div> / <div class="qty-all">  15 </div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-xs-6">
					<div class="box-shop-qty">
						<div class="box-shop-qty">
							<div class="shop_name"> ร้านXXXX</div>
							<div class="shop-qty ">
								<div class="shop-qty-cur">  5  </div> / <div class="qty-all">  15 </div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-xs-6">
					<div class="box-shop-qty">
						<div class="box-shop-qty">
							<div class="shop_name"> ร้านXXXX</div>
							<div class="shop-qty ">
								<div class="shop-qty-cur">  3  </div> / <div class="qty-all">  15 </div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-xs-6">
					<div class="box-shop-qty">
						<div class="box-shop-qty">
							<div class="shop_name"> ร้านXXXX</div>
							<div class="shop-qty ">
								<div class="shop-qty-cur">  8  </div> / <div class="qty-all">  15 </div>
							</div>
						</div>
					</div>
				</div> -->
			</div>
		</div>

		<div class="modal bs-modal-sm  fade baodang-error" id="no-shop" tabindex="-1" role="dialog"  aria-hidden="true">
		    <div class="vertical-alignment-helper">
		        <div class="modal-dialog vertical-align-center ">
		            <div class="modal-content modal-confirm" >
						<div class="modal-body baodang">
							<p class="modal-shop-text">ไม่พบร้านค้า</p>
							<a href="javascript:closeModal();" class="btn-cancle"><img src="/vendor/images/btn-cancle.png"></a>
					    </div>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="modal bs-modal-sm  fade baodang-error" id="over-in-day" tabindex="-1" role="dialog"  aria-hidden="true">
		    <div class="vertical-alignment-helper">
		        <div class="modal-dialog vertical-align-center ">
		            <div class="modal-content modal-confirm" >
						<div class="modal-body baodang">
							<p class="modal-shop-text">ท่านได้รับสิทธิ์สำหรับร้านค้านี้ครบ 2 สิทธิ์แล้วกรุณารับสิทธิ์ได้ใหม่ในวันถัดไป</p>
							<a href="javascript:closeModal();" class="btn-cancle"><img src="/vendor/images/btn-cancle.png"></a>
					    </div>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="modal bs-modal-sm  fade baodang-error" id="confirm" tabindex="-1" role="dialog"  aria-hidden="true">
		    <div class="vertical-alignment-helper">
		        <div class="modal-dialog vertical-align-center ">
		            <div class="modal-content modal-confirm" >
						<div class="modal-body baodang">
							<p class="modal-shop-text">โปรดตรวจสอบชื่อร้านของท่าน</p>
							<div class="modal-shop">ร้านค้า</div>
							<div class="modal-shop-name" id="model-shop-name"></div>
							<a href="javascript:closeModal();" class="btn-cancle"><img src="/vendor/images/btn-cancle.png"></a>
							<a href="javascript:chooseNumberBuy();" class="btn-confirm"><img src="/vendor/images/btn-confirm.png"></a>
					    </div>
		            </div>
		        </div>
		    </div>
		</div>


		<div class="modal bs-modal-sm  fade baodang-error" id="cus-buy" tabindex="-1" role="dialog"  aria-hidden="true">
		    <div class="vertical-alignment-helper">
		        <div class="modal-dialog vertical-align-center ">
		            <div class="modal-content modal-confirm" >
						<div class="modal-body baodang">
							<p class="modal-shop-text">เจ้าของร้านระบุจำนวนที่ลูกค้าซื้อ</p>
			<!-- 				<div class="modal-shop">ร้านค้า</div>
							<div class="modal-shop-name">เฮียยุทธ์</div> -->
							<div class="control">
								<div class="btn-left" ><img src="/vendor/images/btn-left.png"></div>
								<div class="cus-buy-qty">0</div>
								<div class="btn-right"> <img src="/vendor/images/btn-right.png"></div>
							</div>

							<a href="javascript:closeModal();" class="btn-cancle"><img src="/vendor/images/btn-cancle.png"></a>
							<a href="javascript:customerRecievePoint();" class="btn-confirm"><img src="/vendor/images/btn-confirm.png"></a>
					    </div>
		            </div>
		        </div>
		    </div>
		</div>

		<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
		<script>
			function checkShop()
			{
				var shopCode = $('#shop_code').val();
				var registerId = $('#register_id').val();
				$.ajax({
	          		url: "/check-shop?shop_code="+shopCode+"&register_id="+registerId,
	          		success: function( result ) {
	          			console.log(result.response.is_shop);
	          			if(result.response.is_shop == 0){
	          				$('#no-shop').modal('show');
	          			}else if(result.response.is_active_in_day == 0){
	          				$('#over-in-day').modal('show');
	          			}else{
	          				console.log(result.response);
	          				$('#shop_id').val(result.response.shop_id);
	          				$('#customer_qty').val(result.response.customer_qty_in_day);
	          				$('#model-shop-name').empty();
	          				$('#model-shop-name').append(result.response.shop_name);
	          				$('#confirm').modal('show');
	          			}
				    }
		        });
			}

			function chooseNumberBuy()
			{
				closeModal();
			  	$('#cus-buy').modal('show');
			}

			function closeModal()
			{
				$('#confirm').modal('hide');
			  	$('#cus-buy').modal('hide');
			  	$('#no-shop').modal('hide');
			  	$('#over-in-day').modal('hide');
			}

			function customerRecievePoint()
			{
				var qty = $('#qty').val();
				var shopId = $('#shop_id').val();
				var registerId = $('#register_id').val();
				if(qty > 0){
					$.ajax({
		          		url: "/customer-store-code?shop_id="+shopId+"&register_id="+registerId+"&qty="+qty,
		          		success: function( result ) {
		          			window.location.href = '/by-pass-customer-code/'+registerId+'/'+shopId;
		          			// console.log(result);
					    }
			        });
				}else{
					closeModal();
				}
				
			}

			// $( ".box-shop-qty" ).click(function() {
			// 	var cus_name = $( ".shop_name" ).html();
			// 	$('#confirm').modal('show');
			// });

			// $( ".btn-confirm" ).click(function() {
			// 	$('#confirm').modal('hide');
			// 	$('#cus-buy').modal('show');
			// });

			// $( ".btn-cancle" ).click(function() {
			//   	$('#confirm').modal('hide');
			//   	$('#cus-buy').modal('hide');
			//   	$('#no-shop').modal('hide');
			// });

			$( ".btn-left" ).click(function() {
				var qty = $( ".cus-buy-qty" ).html();
				qty = parseInt(qty);

				var qty_new = qty-1;
				if(qty_new < 0 ){
					qty_new = 0;
				}
				$('#qty').val(qty_new);
			    $( ".cus-buy-qty" ).html(qty_new);

			});

			$( ".btn-right" ).click(function() {
				var qtyInday = $('#customer_qty').val();
				var qty = $( ".cus-buy-qty" ).html();
				qty = parseInt(qty);

				var qty_new = qty+1;
				var newQtyInDay = 2-qtyInday;
				if(qty_new > newQtyInDay ){
					qty_new = newQtyInDay;
				}
				$('#qty').val(qty_new);
			    $( ".cus-buy-qty" ).html(qty_new);

			});


		</script>

	</body>
</html>