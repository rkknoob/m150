<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/vendor/css/style.css">
	</head>
	<body class="bg3">
		<div class="shop-02">
			<div  class="banner-shop">
				<img src="/vendor/images/banner-shop.png">
			</div>
			<div class="content">
				<div class="form">
					<img src="/vendor/images/icon-shop-01.png" class="icon-shop">
					<div class="text-shop-1">{{ $shop->shop_name }}</div>
					<div class="text-shop-2">จำนวนที่ลูกค้าสะสมทั้งหมด / ขวด</div>
					<div class="text-shop-3">ตั้งแต่ {{ $startDateEndDate }} </div>
					<form>
						<input type="text" name="" class="input-shop-2" value="{{ $countAll }}">
						<div class="text-shop-4">จำนวนที่แถมไป / ขวด</div>
						<input type="text" name="" class="input-shop-2" value="{{ $countRecieve }}">
					</form>
				</div>
			</div>
		</div>

	</body>
</html>