<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/vendor/css/style.css?v1">
	</head>
	<body class="bg3">
		<div class="shop-02">
			<div  class="banner-shop">
				<img src="/vendor/images/banner-shop.png">
			</div>
			<div class="content">
				<div class="form">
					<img src="/vendor/images/icon-shop-01.png" class="icon-shop">
					<div class="text-shop-1">{{ $shop->shop_name }}</div>
					<form >
						<div class="shop-02-left">
							<p>จำนวนขวดที่ร้านค้า</p>
							<p>ซื้อสะสมทั้งหมด</p>
							<p>ตั้งแต่ {{ $startDateEndDateShopBuyer }}</p>
							<input type="text" name="" class="input-shop-2" value="{{ $shopBuyerSum }}" readonly="">
						</div>
						<div class="shop-02-right">
							<p>จำนวนขวดที่ลูกค้า</p>
							<p>ซื้อสะสมทั้งหมด</p>
							<p>ตั้งแต่ {{ $startDateEndDate }}</p>
							<input type="text" name="" class="input-shop-2" value="{{ $countAll }}" readonly="">
							<div style="display:inline-block;">
								<div class="box-arrow"><img src="/vendor/images/icon-arrow-left.png" class="icon-arrow-left"></div>
								<span   class="text-detail"><a href=/by-pass-shop-cus-de/{{ $shop->id }}" class="link-detail">กดเพื่อดูรายละเอียด</a></span >
								<div class="box-arrow"><img src="/vendor/images/icon-arrow-right.png" class="icon-arrow-right"></div>
							</div>
							<div class="text-shop-4">จำนวนที่แถมไป / ขวด</div>
							<input type="text" name="" class="input-shop-2" value="{{ $countRecieve }}" readonly="">
						</div>
					</form>

				</div>
			</div>
		</div>

	</body>
</html>