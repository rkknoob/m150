@extends('layouts.admin.html5')

@section('head')
    <style type="text/css">
        table {
          display: inline-block;
          overflow-x: auto;
          white-space: nowrap;
          // make fixed table width effected by overflow-x
          max-width: 100%;
          // hide all borders that make rows not filled with the table width
          border: 0;
        }
    </style>
@stop

@section('body')
    <aside class="right-side">
        <!-- Main content -->
        <section class="content-header">
            <h1>Welcome to Yellow Cloud Platform</h1>
            <ol class="breadcrumb">
                <li class="active">
                    <a href="/">
                        <i class="livicon" data-name="register-detail" data-size="14" data-color="#333" data-hovercolor="#333"></i> ข้อมูลร้านค้า
                    </a>
                </li>
            </ol>
        </section>

        <section class="content">
            <div class="form-inline col-md-12" style="margin-bottom: 10px">
                <div class="col-md-2">
                </div>
                <div class="col-md-10 form-inline">
                    <input class="form-control" type="text" id="outlet_code" value="" name="outlet_code" style="margin-right : 10px;" placeholder="Outlet Code" value="{{$outletCode}}" width="100">
                    <select class="form-control" name="" id="" style="margin-right : 10px;">
                        @for($i = $nowYear - 2; $i < $nowYear; $i++)
                            <option value="{{$i}}" @if($i == $nowYear) selected="" @endif>{{$i}}</option>
                        @endfor
                        <option value="{{$nowYear}}" @if($year == $nowYear) selected="" @endif>{{$nowYear}}</option>
                    </select>
                    <div class="input-group">
                        <!-- <div class="input-group-addon">
                            <i class="livicon" data-name="phone" data-size="14" data-loop="true"></i>
                        </div>
                        <input type="text" class="form-control" id="daterange3" value="{{ $date }}" /> -->
                    </div>
                    <a href="#" onclick="searchDate()" class="btn btn-info btn-sm" style="margin-right : 20px; margin-left : 10px;">Search</a>
                    <a href="#" onclick="clearFilter()" class="btn btn-info btn-sm" style="margin-right : 20px;">Clear Filter</a>
                    <a href="" class="btn btn-info btn-sm" target="_blank">Export Excel</a>
                </div>
            </div>
            <div class="portlet box danger">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="livicon" data-name="wifi" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> ข้อมูลร้านค้า
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>Outlet Name</th>
                                    <th>Customer Name</th>
                                    <th>เบอร์โทรศัพท์</th>
                                    <th>Shop Type</th>
                                    <th>Outlet Code</th>
                                    <th>Area</th>
                                    <th>Jan</th>
                                    <th>Feb</th>
                                    <th>Mar</th>
                                    <th>Apr</th>
                                    <th>May</th>
                                    <th>Jun</th>
                                    <th>Jul</th>
                                    <th>Aug</th>
                                    <th>Sep</th>
                                    <th>Oct</th>
                                    <th>Nov</th>
                                    <th>Dec</th>
                                    <th>ยอดรวม</th>
                                    <th>วันที่แก้ไขล่าสุด</th>
                                    <!-- <th>สถานะ</th> -->
                                    <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($datas as $key => $data)
                                    <?php 
                                        $number = ($datas->currentPage()-1)*10; 
                                        $number =  $number+($key+1);
                                    ?>
                                    <tr>
                                        <td>{{ $number }}</td>
                                        <td>{{ $data->shop_name }}</td>
                                        <td>{{ $data->shop_customer_name }}</td>
                                        <td>{{ $data->shop_tel }}</td>
                                        <td>{{ $data->shop_type }}</td>
                                        <td>{{ $data->shop_code }}</td>
                                        <td>{{ $data->shop_area }}</td>
                                        <td>{{ ($data->january == '')? '-' : $data->january }}</td>
                                        <td>{{ ($data->february == '')? '-' : $data->february }}</td>
                                        <td>{{ ($data->march == '')? '-' : $data->march }}</td>
                                        <td>{{ ($data->april == '')? '-' : $data->april }}</td>
                                        <td>{{ ($data->may == '')? '-' : $data->may }}</td>
                                        <td>{{ ($data->june == '')? '-' : $data->june }}</td>
                                        <td>{{ ($data->july == '')? '-' : $data->july }}</td>
                                        <td>{{ ($data->august == '')? '-' : $data->august }}</td>
                                        <td>{{ ($data->september == '')? '-' : $data->september }}</td>
                                        <td>{{ ($data->october == '')? '-' : $data->october }}</td>
                                        <td>{{ ($data->november == '')? '-' : $data->november }}</td>
                                        <td>{{ ($data->december == '')? '-' : $data->december }}</td>
                                        <td>{{ ($data->grand_total == '')? '-' : $data->grand_total }}</td>
                                        <td>{{ $data->shop_buyer_updated_at }}</td>
                                        <!-- <td></td> -->
                                        <!-- <td></td> -->
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! $datas->appends(['date' => $date,'outlet_code' => $outletCode,'year' => $year])->links('vendor.pagination.bootstrap-4') !!}
                </div>
            </div>
        </section>
    </aside>
    <script type="text/javascript">
        function searchDate()
        {
            var outletCode = $('#outlet_code').val();
            var year = $('#year').val();
            var date = $('#daterange3').val();
            window.location.href = "admin-shop-detail?outlet_code="+outletCode+"&date="+date+"&area="+year;
        }

        function clearFilter()
        {
            window.location.href = "admin-shop-detail?outlet_code=&date=&year=";
        }
    </script>
@stop