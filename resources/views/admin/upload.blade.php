@extends('layouts.admin.html5')

@section('head')
@stop

@section('body')
    <aside class="right-side">
        <!-- Main content -->
        <section class="content-header">
            <h1>Welcome to Yellow Cloud Platform</h1>
            <ol class="breadcrumb">
                <li class="active">
                    <a href="/">
                        <i class="livicon" data-name="register-detail" data-size="14" data-color="#333" data-hovercolor="#333"></i> Upload File
                    </a>
                </li>
            </ol>
        </section>

        <section class="content">
            <div class="form-inline col-md-12" style="margin-bottom: 10px">
                
            </div>
            <div class="portlet box danger">
                @if($status == 'upload-page')
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="livicon" data-name="wifi" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Upload File
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{action('AdminController@uploadDataStore')}}" method="post" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <fieldset>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                            <div class="form-control" data-trigger="fileinput">
                                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                                <span class="fileinput-filename"></span>
                                            </div>
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">Select file</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="file_data"></span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control" name="year">
                                            @for($i = $nowYear - 2; $i < $nowYear; $i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                            <option value="{{$nowYear}}" selected="">{{$nowYear}}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-responsive button-alignment btn-primary" style="margin-bottom:7px;">Upload</button>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                @else
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h4 class="panel-title">Upload Success</h4>
                            <span class="pull-right">
                                <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                                <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <p>
                                Upload Data Success
                            </p>
                        </div>
                    </div>
                @endif
            </div>
        </section>
    </aside>
    <script type="text/javascript">
        
    </script>
@stop