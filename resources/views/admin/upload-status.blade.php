@extends('layouts.admin.html5')

@section('head')
@stop

@section('body')
    <aside class="right-side">
        <!-- Main content -->
        <section class="content-header">
            <h1>Welcome to Yellow Cloud Platform</h1>
            <ol class="breadcrumb">
                <li class="active">
                    <a href="/">
                        <i class="livicon" data-name="register-detail" data-size="14" data-color="#333" data-hovercolor="#333"></i> Upload Status
                    </a>
                </li>
            </ol>
        </section>

        <section class="content">
            <div class="form-inline col-md-12" style="margin-bottom: 10px">
                <div class="col-md-2">
                </div>
                <div class="col-md-10 form-inline">
                </div>
            </div>
            <div class="portlet box danger">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="livicon" data-name="wifi" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> Upload Status
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>File</th>
                                    <th>Created Date</th>
                                    <th>Updated Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($datas as $key => $data)
                                    <?php 
                                        $number = ($datas->currentPage()-1)*10; 
                                        $number =  $number+($key+1);
                                    ?>
                                    <tr>
                                        <td class="text-center">{{ $number }}</td>
                                        <td class="text-center">{{ $data->file_name }}</td>
                                        <td class="text-center">{{ $data->created_at->format('d/m/y H:i:s') }}</td>
                                        <td class="text-center">{{ $data->updated_at->format('d/m/y H:i:s') }}</td>
                                        @if($data->status == 'Success')
                                            <td class="text-center"><span style="background-color: #32CD32">{{ $data->status }}</span></td>
                                        @else
                                            <td class="text-center"><span style="background-color: #C3C3C3">{{ $data->status }}</span></td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! $datas->links('vendor.pagination.bootstrap-4') !!}
                </div>
            </div>
        </section>
    </aside>
@stop