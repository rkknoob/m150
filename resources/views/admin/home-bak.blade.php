@extends('layouts.admin.html5')

@section('head')
@stop

@section('body')
    <aside class="right-side">
        <!-- Main content -->
        <section class="content-header">
            <h1>Welcome to Yellow Cloud Platform</h1>
            <ol class="breadcrumb">
                <li class="active">
                    <a href="#">
                        <i class="livicon" data-name="home" data-size="14" data-color="#333" data-hovercolor="#333"></i> Home
                    </a>
                </li>
            </ol>
        </section>

        <div class="alert alert-success alert-dismissable margin5">
            <strong>ผู้บริโภค:</strong>
        </div>

        <section class="content">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 margin_10">
                    <!-- Trans label pie charts strats here-->
                    <div class="lightbluebg no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 text-right">
                                        <span>จำนวนผู้บริโภคที่ลงทะเบียน</span> <!-- จำนวนคนที่ลงทะเบียนทั้งหมด -->
                                        <div class="number" id="myTargetElement1"></div>
                                    </div>
                                    <i class="livicon  pull-right" data-name="eye-open" data-l="true" data-c="#fff" data-hc="#fff" data-s="70"></i>
                                </div>                                  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 margin_10">
                    <!-- Trans label pie charts strats here-->
                    <div class="redbg no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 pull-left">
                                        <span>จำนวนขวดสะสมทั้งหมด</span> <!-- จำนวนคนที่มีการสะสม ตั้งแต่ 1 ขวด -->
                                        <div class="number" id="myTargetElement2"></div>
                                    </div>
                                    <i class="livicon pull-right" data-name="star-empty" data-l="true" data-c="#fff" data-hc="#fff" data-s="70"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 margin_10">
                    <!-- Trans label pie charts strats here-->
                    <div class="redbg no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 pull-left">
                                        <span>จำนวนผู้บริโภคที่ Redeem</span> <!-- จำนวนคนที่มีการสะสม ตั้งแต่ 1 ขวด -->
                                        <div class="number" id="myTargetElement3"></div>
                                    </div>
                                    <i class="livicon pull-right" data-name="shopping-cart" data-l="true" data-c="#fff" data-hc="#fff" data-s="70"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-6 margin_10">
                    <!-- Trans label pie charts strats here-->
                    <div class="goldbg no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 pull-left">
                                        <span>ผู้บริโภค ที่มีการกลับมาซื้อซ้ำ</span> <!-- จำนวนคนที่สะสม มากกว่า 1 ขวดขึ้นไป -->
                                        <div class="number" id="myTargetElement4"></div>
                                    </div>
                                    <i class="livicon pull-right" data-name="retweet" data-l="true" data-c="#fff" data-hc="#fff" data-s="70"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="alert alert-success alert-dismissable margin5">
                <strong> ร้านค้า:</strong>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 margin_10">
                    <!-- Trans label pie charts strats here-->
                    <div class="lightbluebg no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 text-right">
                                        <span>ยอดร้านค้าที่มีจำนวนสะสม</span> <!-- จำนวร้าน้าท่มีการแจกตั้งแต่ มากกว่าหรือเท่ากับ 1 ขวด -->
                                        <div class="number" id="myTargetElement5"></div>
                                    </div>
                                    <i class="livicon  pull-right" data-name="check-circle" data-l="true" data-c="#fff" data-hc="#fff" data-s="70"></i>
                                </div>                                  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 margin_10">
                    <!-- Trans label pie charts strats here-->
                    <div class="lightbluebg no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 text-right">
                                        <span>จำนวนร้านค้าที่ถูก Redeem</span> <!-- จำนวร้าน้าท่มีการแจกตั้งแต่ มากกว่าหรือเท่ากับ 1 ขวด -->
                                        <div class="number" id="myTargetElement6"></div>
                                    </div>
                                    <i class="livicon  pull-right" data-name="trophy" data-l="true" data-c="#fff" data-hc="#fff" data-s="70"></i>
                                </div>                                  
                            </div>
                        </div>
                    </div>
                </div>                    
            </div>            
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-lg-6">
                    <!-- toggling series charts strats here-->
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="linechart" data-size="16" data-loop="true" data-c="#fff" data-hc="#fff"></i> จำนวนร้านค้าที่ถูก Redeem by จังหวัด
                            </h3>
                            <span class="pull-right">
                                <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                                <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <div id="bar-chart" class="flotChart"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <i class="livicon" data-name="piechart" data-size="16" data-loop="true" data-c="#fff" data-hc="#fff"></i> กลุ่มอายุของลูกค้าที่มาเล่นโปรโมชั่น
                            </h3>
                            <span class="pull-right">
                                <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                                <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <div id="pie1"></div>
                        </div>
                    </div>
                </div>                                      
            </div> 
        </section>
    </aside>
@stop