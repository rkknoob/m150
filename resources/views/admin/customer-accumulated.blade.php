@extends('layouts.admin.html5')

@section('head')
@stop

@section('body')
    <aside class="right-side">
        <!-- Main content -->
        <section class="content-header">
            <h1>Welcome to Yellow Cloud Platform</h1>
            <ol class="breadcrumb">
                <li class="active">
                    <a href="/">
                        <i class="livicon" data-name="register-detail" data-size="14" data-color="#333" data-hovercolor="#333"></i> ลูกค้าที่มียอดสะสม
                    </a>
                </li>
            </ol>
        </section>

        <section class="content">
            <div class="form-inline col-md-12" style="margin-bottom: 10px">
                <div class="col-md-2">
                </div>
                <div class="col-md-10 form-inline">
                    <input class="form-control" type="text" id="name" value="{{ $name }}" name="name" style="margin-right : 10px;" placeholder="ชื่อ" width="100">
                    <input class="form-control" type="text" id="tel" value="{{ $tel }}" name="tel" style="margin-right : 10px;" placeholder="เบอร์โทรศัพท์" width="100">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="livicon" data-name="phone" data-size="14" data-loop="true"></i>
                        </div>
                        <input type="text" class="form-control" id="daterange3" value="{{ $date }}" />
                    </div>
                    <!-- <input id="datepicker" value="{{ $date }}" width="200"/> -->
                    <a href="#" onclick="searchDate()" class="btn btn-info btn-sm" style="margin-right : 20px; margin-left : 10px;">Search</a>
                    <a href="#" onclick="clearFilter()" class="btn btn-info btn-sm" style="margin-right : 20px;">Clear Filter</a>
                    <a href="/admin-customer-accumulated-export?name={{$name}}&tel={{$tel}}&date={{$date}}" class="btn btn-info btn-sm" target="_blank">Export Excel</a>
                </div>
            </div>
            <div class="portlet box danger">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="livicon" data-name="wifi" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> ลูกค้าที่มียอดสะสม
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>ชื่อ</th>
                                    <th>นามสกุล</th>
                                    <th>เบอร์โทรศัพท์</th>
                                    <th>ปีเกิด</th>
                                    <th>เพศ</th>
                                    <th>จำนวน(ขวด)</th>
                                    <th>วันที่สะสม</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($datas as $key => $data)
                                    <?php 
                                        $number = ($datas->currentPage()-1)*10; 
                                        $number =  $number+($key+1);
                                    ?>
                                    <tr>
                                        <td class="text-center">{{ $number }}</td>
                                        <td class="text-center">{{ $data->first_name }}</td>
                                        <td class="text-center">{{ $data->last_name }}</td>
                                        <td class="text-center">{{ $data->phone_number }}</td>
                                        <td class="text-center">{{ $data->year_birth }}</td>
                                        <td class="text-center">{{ $data->gender }}</td>
                                        <td class="text-center">{{ $data->total }}</td>
                                        <td class="text-center">{{ $data->first_create }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! $datas->appends(['date' => $date,'name' => $name,'tel' => $tel])->links('vendor.pagination.bootstrap-4') !!}
                </div>
            </div>
        </section>
    </aside>
    <script type="text/javascript">
        function searchDate()
        {
            var name = $('#name').val();
            var tel = $('#tel').val();
            var date = $('#daterange3').val();
            window.location.href = "admin-customer-accumulated?name="+name+"&date="+date+"&tel="+tel;
        }

        function clearFilter()
        {
            window.location.href = "admin-customer-accumulated?name=&date=&tel=";
        }
    </script>
@stop