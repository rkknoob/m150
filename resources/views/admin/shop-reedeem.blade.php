@extends('layouts.admin.html5')

@section('head')
@stop

@section('body')
    <aside class="right-side">
        <!-- Main content -->
        <section class="content-header">
            <h1>Welcome to Yellow Cloud Platform</h1>
            <ol class="breadcrumb">
                <li class="active">
                    <a href="/">
                        <i class="livicon" data-name="register-detail" data-size="14" data-color="#333" data-hovercolor="#333"></i> ร้านค้าที่ถูก Redeem
                    </a>
                </li>
            </ol>
        </section>

        <section class="content">
            <div class="form-inline col-md-12" style="margin-bottom: 10px">
                <div class="col-md-2">
                </div>
                <div class="col-md-10 form-inline">
                    <input class="form-control" type="text" id="outlet_code" value="{{ $outletCode }}" name="outlet_code" style="margin-right : 10px;" placeholder="Outlet Code" width="100">
                    <input class="form-control" type="text" id="area" value="{{ $area }}" name="area" style="margin-right : 10px;" placeholder="Area" width="100">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="livicon" data-name="phone" data-size="14" data-loop="true"></i>
                        </div>
                        <input type="text" class="form-control" id="daterange3" value="{{ $date }}" />
                    </div>
                    <!-- <input id="datepicker" value="{{ $date }}" width="200"/> -->
                    <a href="#" onclick="searchDate()" class="btn btn-info btn-sm" style="margin-right : 20px; margin-left : 10px;">Search</a>
                    <a href="#" onclick="clearFilter()" class="btn btn-info btn-sm" style="margin-right : 20px;">Clear Filter</a>
                    <a href="/admin-shop-reedeem-export?outlet_code={{$outletCode}}&area={{$area}}&date={{$date}}" class="btn btn-info btn-sm" target="_blank">Export Excel</a>
                </div>
            </div>
            <div class="portlet box danger">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="livicon" data-name="wifi" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i> ร้านค้าที่ถูก Redeem
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>Outlet Name</th>
                                    <th>Customer Name</th>
                                    <th>เบอร์โทรศัพท์</th>
                                    <th>Shop Type</th>
                                    <th>Outlet Code</th>
                                    <th>Area</th>
                                    <th>ยอด Redeem</th>
                                    <th>ยอดสะสมล่าสุด</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($datas as $key => $data)
                                    <?php 
                                        $number = ($datas->currentPage()-1)*10; 
                                        $number =  $number+($key+1);
                                    ?>
                                    <tr>
                                        <td class="text-center">{{ $number }}</td>
                                        <td class="text-center">{{ $data->shop_name }}</td>
                                        <td class="text-center">{{ $data->shop_customer_name }}</td>
                                        <td class="text-center">{{ $data->shop_tel }}</td>
                                        <td class="text-center">{{ $data->shop_type }}</td>
                                        <td class="text-center">{{ $data->shop_code }}</td>
                                        <td class="text-center">{{ $data->shop_area }}</td>
                                        <td class="text-center">{{ $data->total_redeem }}</td>
                                        <td class="text-center">{{ $data->last_update }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {!! $datas->appends(['date' => $date,'outlet_code' => $outletCode,'area' => $area])->links('vendor.pagination.bootstrap-4') !!}
                </div>
            </div>
        </section>
    </aside>
    <script type="text/javascript">
        function searchDate()
        {
            var outletCode = $('#outlet_code').val();
            var area = $('#area').val();
            var date = $('#daterange3').val();
            window.location.href = "admin-shop-reedeem?outlet_code="+outletCode+"&date="+date+"&area="+area;
        }

        function clearFilter()
        {
            window.location.href = "admin-shop-reedeem?outlet_code=&date=&area=";
        }
    </script>
@stop