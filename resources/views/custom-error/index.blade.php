<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">
		<link rel="stylesheet" href="/vendor/css/style.css">
	</head>
	<body class="bg10">
		<div class="shop-02">
			<div  class="banner-shop">
				<img src="/vendor/images/banner-shop.png">
				<div class="rec-center">
		          <img src="/vendor/images/Logo-M150.jpg" class=" img-circle" >
		        </div>
			</div>

			<div class="cus_name_p9"></div>
			<div class="box" style="margin-top: 100px">
				<div>พบข้อผิดพลาดในการใช้งาน</div>
				<div><a href="/"><img src="/vendor/images/btn-home.png" class="btn-home"></a></div>
			</div>
		</div>

	</body>
</html>