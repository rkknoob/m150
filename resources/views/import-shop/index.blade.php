<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>M150</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="col-md-12 text-center">
  		<h1>M150 Import Shop</h1>
  	</div>
  	<form action="{{ action('ShopController@importFile') }}" method="POST" id="validator" role="form" enctype="multipart/form-data">
		{!! csrf_field() !!}
	  	<div class="col-md-12 text-center">
	  		<div class="col-md-5">
	  		</div>
	  		<div class="col-md-2">
	  			<input type="file" name="file_data">
	  		</div>
			<div class="col-md-5">
	  		</div>
	  	</div>
	  	<div class="col-md-12 form-inline text-center" style="margin-top: 20px">
	  		<div class="col-md-5">
  			</div>
  			<div class="col-md-2">
  				<button type="submit" class="btn btn-primary">SAVE</button>
  			</div>
  			<div class="col-md-5">
  			</div>
	  	</div>
  	</form>
    <!-- <div class="col-md-12 form-inline">
    	<div class="col-md-6">
    		
    	</div>
    	<div class="col-md-6">
    		flgfdklgfdjkgjkdlfjgkldfjkgfdldfljg
    	</div>
    </div> -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>