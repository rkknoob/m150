<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/vendor/css/style.css?v1">
	</head>
	<body class="bg-howto">
		<div class="shop-02">
			<div  class="banner-howto">
				<img src="/vendor/images/banner-howto.png">
			</div>
			<div class="content">
				<img src="/vendor/images/howto.png" class="howto">
			</div>
			<a href="/"><img src="/vendor/images/btn-home.png" class="btn-home"></a>
		</div>

	</body>
</html>