<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">
		<link rel="stylesheet" href="/vendor/css/style.css">
	</head>
	<body class="bg3">
		<div class="shop-01">
			<div  class="banner-shop">
				<img src="/vendor/images/banner-shop.png">
			</div>
			<div class="content">
				<div class="form">
					<img src="/vendor/images/icon-shop-01.png" class="icon-shop">
					<div class="title-form">โปรดกรอกรหัสร้านค้าของท่าน</div>
					<form>
						<input type="text" name="shop_code" id="shop_code" class="input-shop">
						<button type="button" class="btn-confirm btn-tran" onclick="checkShop()"> <img src="/vendor/images/btn-confirm.png"> </button>
					</form>
				</div>
			</div>
		</div>

		<div class="modal bs-modal-sm  fade baodang-error" id="no-shop" tabindex="-1" role="dialog"  aria-hidden="true">
		    <div class="vertical-alignment-helper">
		        <div class="modal-dialog vertical-align-center ">
		            <div class="modal-content modal-confirm" >
						<div class="modal-body baodang">
							<p class="modal-shop-text">โปรดตรวจสอบรหัสร้านค้าของท่าน</p>
							<a href="javascript:closeModal();" class="btn-cancle"><img src="/vendor/images/btn-cancle.png"></a>
					    </div>
		            </div>
		        </div>
		    </div>
		</div>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
		<script type="text/javascript">
			function closeModal()
			{
				$('#confirm').modal('hide');
			  	$('#cus-buy').modal('hide');
			  	$('#no-shop').modal('hide');
			  	$('#over-in-day').modal('hide');
			}

			function checkShop()
			{
				var shopCode = $('#shop_code').val();
				$.ajax({
	          		url: "/check-shop-only?shop_code="+shopCode,
	          		success: function( result ) {
	          			if(result.response.is_shop == 0){
	          				$('#no-shop').modal('show');
	          			}else{
	          				var shopId = result.response.shop_id;
	          				window.location.href = '/by-pass-shop/'+shopId;
	          			}
				    }
		        });
			}
		</script>
	</body>
</html>