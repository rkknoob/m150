<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>M150</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.min.css">
		<link rel="stylesheet" href="/vendor/css/style.css">
	</head>
	<body >
		<div class="content-p10 bg10">
			<img src="/vendor/images/text-p10.png" class="text-p10">
			<div class="p-10">
				<img src="/vendor/images/toon.png" class="bg-p10-new">
			</div>
			<div class="p10-center">
				<div class="box-left">
					<img src="/vendor/images/icon-shop-01.png">
					<div class="res_name">{{ $shopCustomer->shop->shop_name }}</div>
				</div>
				<div class="box-clock">
					<img src="/vendor/images/clock.png" class="p10-clock">
					<div class="p10-time" id="time">30.00</div>
				</div>
				<div class="box-btn-shop-gift">
					<button onclick="location.href = '/by-pass/{{ $registerData->id }}';" class="btn-shop-gift"><img src="/vendor/images/btn-shop-gift.png" class=""></button>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				var thirtyMinutes = 60 * 30,
			    display = document.querySelector('#time');
			    startTimer(thirtyMinutes, display);
			});
			function startTimer(duration, display) {
			    var timer = duration, minutes, seconds;
			    setInterval(function () {
			        minutes = parseInt(timer / 60, 10)
			        seconds = parseInt(timer % 60, 10);

			        minutes = minutes < 10 ? "0" + minutes : minutes;
			        seconds = seconds < 10 ? "0" + seconds : seconds;

			        display.textContent = minutes + ":" + seconds;

			        if (--timer < 0) {
			            timer = duration;
			        }
			    }, 1000);
			}
		</script>
	</body>
</html>

<!--     position: absolute;
    top: 40%; -->