<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimShopBuyerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_shop_buyer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shop_code')->nullable();
            $table->string('shop_id')->nullable();
            $table->string('shop_name')->nullable();
            $table->string('shop_customer_name')->nullable();
            $table->string('shop_tel')->nullable();
            $table->string('shop_type')->nullable();
            $table->string('year')->nullable();
            $table->string('january')->nullable();
            $table->string('shop_area')->nullable();
            $table->string('february')->nullable();
            $table->string('march')->nullable();
            $table->string('april')->nullable();
            $table->string('may')->nullable();
            $table->string('june')->nullable();
            $table->string('august')->nullable();
            $table->string('september')->nullable();
            $table->string('october')->nullable();
            $table->string('november')->nullable();
            $table->string('december')->nullable();
            $table->string('grand_total')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_shop_buyer');
    }
}
