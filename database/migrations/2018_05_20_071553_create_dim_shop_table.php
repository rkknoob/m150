<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_shop', function (Blueprint $table) {
            $table->increments('id');
            $table->string('shop_code')->nullable();
            $table->string('shop_name')->nullable();
            $table->string('shop_customer_name')->nullable();
            $table->string('shop_tel')->nullable();
            $table->string('shop_type')->nullable();
            $table->string('shop_area')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_shop');
    }
}
