<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimShopCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_shop_customer', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('register_id');
            $table->integer('shop_id');
            $table->boolean('is_active')->default(0);
            $table->boolean('is_recieve')->default(0);
            $table->dateTime('recieve_date')->nullable();
            $table->integer('total')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_shop_customer');
    }
}
