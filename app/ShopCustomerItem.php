<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopCustomerItem extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fact_shop_customer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_customer_id',
    ];
}
