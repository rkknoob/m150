<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopBuyer extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_shop_buyer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_code',
        'shop_id',
        'shop_name',
        'shop_customer_name',
        'shop_tel',
        'shop_type',
        'shop_area',
        'year',
        'january',
        'february',
        'march',
        'april',
        'may',
        'june',
        'july',
        'august',
        'september',
        'october',
        'november',
        'december',
		'grand_total',
    ];
}
