<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\ShopCustomer;
use App\ShopCustomerItem;
use App\RegisterData;
use App\LineUserProfile;
use Carbon\Carbon;

class CustomerShopController extends Controller
{
    public function customerShopPage()
    {
        $datas = [];
        $id = \Session::get('register-id', '');
        $shopId = \Session::get('shop-id', '');
        \Session::put('register-id', '');
        \Session::put('shop-id', '');
        if($id == ''){
            return redirect('/error-page');
        }
        $registerData = RegisterData::find($id);
        $lineUserProfile = LineUserProfile::find($registerData->line_user_id);
        $shopCustomer = ShopCustomer::where('register_id',$registerData->id)->where('shop_id',$shopId)->where('is_active',1)->first();

        $row = 0;
        $column = 0;
        $stamp = 0;
        $customerCount = $shopCustomer->total;
        for ($i=0; $i < 14; $i++) {
            $active = 0;

            if($column == 5){
                $column = 0;
                $row++;
            }
            if($customerCount > 0){
                $active = 1;
            }else{
                $active = 0;
            }
            $datas[$row][$column] = $active;


            $stamp++;
            $column++;
            if($customerCount > 0){
                $customerCount--;
            }
        }

        return view('customer-shop.index')
            ->with('registerData',$registerData)
            ->with('shopCustomer',$shopCustomer)
            ->with('datas',$datas)
            ->with('lineUserProfile',$lineUserProfile);
    }

    public function byPassWaitingRecieve($id,$shopId)
    {
        $registerData = RegisterData::find($id);
        if(!$registerData){
            return redirect('/error-page');
        }

        \Session::put('register-id', $registerData->id);
        \Session::put('shop-id', $shopId);
        return redirect('/customer-shop-recieve-wait');
    }

    public function waitingReceivePage()
    {
        $id = \Session::get('register-id', '');
        $shopId = \Session::get('shop-id', '');
        \Session::put('register-id', '');
        \Session::put('shop-id', '');
        if($id == ''){
            return redirect('/error-page');
        }
        $registerData = RegisterData::find($id);
        $lineUserProfile = LineUserProfile::find($registerData->line_user_id);
        $shopCustomer = ShopCustomer::where('register_id',$registerData->id)->where('shop_id',$shopId)->where('is_active',1)->first();

        return view('waiting-recieve.index')
            ->with('registerData',$registerData)
            ->with('shopCustomer',$shopCustomer)
            ->with('lineUserProfile',$lineUserProfile);
    }

    public function customerShopRecieve(Request $request)
    {
        $registerId = $request->register_id;
        $shopId = $request->shop_id;
        $shopCustomer = ShopCustomer::where('register_id',$registerId)->where('shop_id',$shopId)->where('is_active',1)->first();
        $dateNow = Carbon::now()->format('Y-m-d H:i:s');
        $shopCustomer->update([
            'is_active' => 0,
            'is_recieve' => 1,
            'recieve_date' => $dateNow
        ]);

        $shopCustomer = ShopCustomer::create([
            'register_id' => $registerId,
            'shop_id' => $shopId,
            'is_active' => 1,
            'total' => 0
        ]);

        return response()->json([
            'response' => 1,
        ]);
    }

    public function byPassRecieve($id,$shopId)
    {
        $registerData = RegisterData::find($id);
        if(!$registerData){
            return redirect('/error-page');
        }

        \Session::put('register-id', $registerData->id);
        \Session::put('shop-id', $shopId);
        return redirect('/customer-recieve');
    }

    public function customerRecievePage()
    {
        $id = \Session::get('register-id', '');
        $shopId = \Session::get('shop-id', '');
        \Session::put('register-id', '');
        \Session::put('shop-id', '');
        if($id == ''){
            return redirect('/error-page');
        }
        $registerData = RegisterData::find($id);
        $lineUserProfile = LineUserProfile::find($registerData->line_user_id);
        $shopCustomer = ShopCustomer::where('register_id',$registerData->id)->where('shop_id',$shopId)->where('is_active',1)->first();

        return view('customer-recieve.index')
            ->with('registerData',$registerData)
            ->with('shopCustomer',$shopCustomer)
            ->with('lineUserProfile',$lineUserProfile);
    }
}
