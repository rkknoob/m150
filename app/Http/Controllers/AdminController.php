<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\ShopCustomer;
use App\ShopCustomerItem;
use App\ShopBuyer;
use App\RegisterData;
use App\UploadData\UploadData;
use App\LoginData\User;
use Carbon\Carbon;
use Excel;
use Jenssegers\Agent;

class AdminController extends Controller
{
    public function homePage()
    {
        $agent = new \Jenssegers\Agent\Agent;

        $result = $agent->isMobile();



        if(!array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-login');
        }
        $shopCustomers = ShopCustomer::all();
        $countCustomerRedeem = $shopCustomers->filter(function ($value, $key) {
            return $value->is_recieve == 1;
        })->count();
        $shopAllCollect = $shopCustomers->groupBy('shop_id')->count();
        $shopRedeem = $shopCustomers->filter(function ($value, $key) {
            return $value->is_recieve == 1;
        })->groupBy('shop_id')->count();
        $allRegisterData = RegisterData::all();
        $customerReturn = \DB::table('fact_shop_customer as fsc')
            ->select(
                \DB::Raw('count(fsc.id) as total')
            )
            ->leftjoin('dim_shop_customer as dsc','fsc.shop_customer_id','=','dsc.id')
            ->groupBy('dsc.register_id')->get();
        $customerReturnCount = $customerReturn->filter(function ($value, $key) {
            return $value->total >= 2;
        })->count();
        $shopByProvinces = \DB::table('dim_shop_customer as sc')
            ->select(
                's.shop_area',
                \DB::raw('count(*) as total')
            )
            ->leftjoin('dim_shop as s','sc.shop_id','=','s.id')
            ->where('sc.is_recieve',1)
            ->groupBy('shop_area')->get();
        $shopProvinces = Shop::select(
            'shop_area',
            \DB::raw('count(*) as total')
        )->groupBy('shop_area')->get();
        $page = "home";
        $datas = [];
        $datas['age'] = [];
        $datas['shop'] = [];
        $datas['shop']['redeem'] = [];
        $datas['shop']['redeem'][1] = 0;
        $datas['shop']['redeem'][2] = 0;
        $datas['shop']['redeem'][3] = 0;
        $datas['shop']['redeem'][4] = 0;
        $datas['shop']['all'] = [];
        $datas['shop']['all'][1] = 0;
        $datas['shop']['all'][2] = 0;
        $datas['shop']['all'][3] = 0;
        $datas['shop']['all'][4] = 0;
        $datas['register_count'] = 0;
        $datas['bottle_all'] = 0;
        $datas['customer_redeem'] = 0;
        $datas['shop_collect'] = 0;
        $datas['shop_redeem'] = 0;
        $datas['customer_return'] = 0;
        $datas['register_count'] = RegisterData::all()->count();
        $datas['bottle_all'] = $shopCustomers->sum('total');
        $datas['customer_redeem'] = $countCustomerRedeem;
        $datas['shop_collect'] = $shopAllCollect;
        $datas['shop_redeem'] = $shopRedeem;
        $datas['age']['1'] = $allRegisterData->filter(function ($value, $key) {
            return intval($value->year_birth) > 2546;
        })->count();
        $datas['age']['2'] = $allRegisterData->filter(function ($value, $key) {
            return intval($value->year_birth) >= 2531 && intval($value->year_birth) <= 2546;
        })->count();
        $datas['age']['3'] = $allRegisterData->filter(function ($value, $key) {
            return intval($value->year_birth) >= 2501 &&  intval($value->year_birth) < 2531;
        })->count();
        $datas['age']['4'] = $allRegisterData->filter(function ($value, $key) {
            return intval($value->year_birth) >= 2471 &&  intval($value->year_birth) < 2501;
        })->count();
        $datas['age']['5'] = $allRegisterData->filter(function ($value, $key) {
            return intval($value->year_birth) < 2486;
        })->count();
        $datas['customer_return'] = $customerReturnCount;
        foreach ($shopByProvinces as $key => $shopByProvince) {
            $key = 0;
            if($shopByProvince->shop_area == 'กรุงเทพมหานคร'){
                $key = 1;
            }else if($shopByProvince->shop_area == 'จ.สุพรรณบุรี'){
                $key = 2;
            }else if($shopByProvince->shop_area == 'จ.อ่างทอง'){
                $key = 3;
            }else if($shopByProvince->shop_area == 'จ.สมุทรปราการ'){
                $key = 4;
            }

            if($key != 0){
                $datas['shop']['redeem'][$key] = $shopByProvince->total;
            }
        }

        foreach ($shopProvinces as $key => $shopProvince) {
            $key = 0;
            if($shopProvince->shop_area == 'กรุงเทพมหานคร'){
                $key = 1;
            }else if($shopProvince->shop_area == 'จ.สุพรรณบุรี'){
                $key = 2;
            }else if($shopProvince->shop_area == 'จ.อ่างทอง'){
                $key = 3;
            }else if($shopProvince->shop_area == 'จ.สมุทรปราการ'){
                $key = 4;
            }

            if($key != 0){
                $datas['shop']['all'][$key] = $shopProvince->total;
            }
        }

        return view('admin.home')
            ->with('datas',$datas)
            ->with('page',$page);
    }

    public function registerDetailPage(Request $request)
    {
        if(!array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-login');
        }
        $page = "register-detail";

        $date = $request->date;
        $phoneNumber = $request->phone_number;
        $firstName = $request->first_name;
        $startDate = "";
        $endDate = "";
        if($date == ''){
            $startDate = Carbon::now()->format('m/d/Y');
            $endDate = Carbon::now()->format('m/d/Y');
        }else{
            $explodeDate = explode(' - ', $date);
            $startDate = $explodeDate[0];
            $endDate = $explodeDate[1];
        }
        $datas = \DB::table('dim_register as r')
            ->select(
                'r.first_name',
                'r.last_name',
                'r.year_birth',
                'r.gender',
                'r.phone_number',
                \DB::raw('DATE_FORMAT(r.created_at, "%d/%m/%Y %H:%i:%s") as created_date')
            );
        if($startDate != "" && $endDate != ""){
            $datas = $datas->where(\DB::raw('DATE_FORMAT(r.created_at, "%m/%d/%Y")'),'>=',$startDate)->where(\DB::raw('DATE_FORMAT(r.created_at, "%m/%d/%Y")'),'<=',$endDate);
        }
        if($phoneNumber != ""){
            $datas = $datas->where('r.phone_number',$phoneNumber);
        }
        if($firstName != ""){
            $datas = $datas->where('r.first_name',$firstName);
        }
        $datas = $datas->orderByDesc('r.created_at')
            ->paginate(10);
        // ->paginate(10);

        return view('admin.register-detail')
            ->with('page',$page)
            ->with('date',$date)
            ->with('phoneNumber',$phoneNumber)
            ->with('firstName',$firstName)
            ->with('datas',$datas);
    }

    public function registerDetailPageExport(Request $request)
    {
        $date = $request->date;
        $phoneNumber = $request->phone_number;
        $firstName = $request->first_name;
        $startDate = "";
        $endDate = "";
        if($date == ''){
            $startDate = Carbon::now()->format('m/d/Y');
            $endDate = Carbon::now()->format('m/d/Y');
        }else{
            $explodeDate = explode(' - ', $date);
            $startDate = $explodeDate[0];
            $endDate = $explodeDate[1];
        }
        $datas = \DB::table('dim_register as r')
            ->select(
                'r.first_name as ชื่อ',
                'r.last_name as นามสกุล',
                'r.year_birth as เบอร์โทรศัพท์',
                'r.gender as ปีเกิด',
                'r.phone_number as เพศ',
                \DB::raw('DATE_FORMAT(r.created_at, "%d/%m/%Y %H:%i:%s") as วันที่ลงทะเบียน')
            );
        if($startDate != "" && $endDate != ""){
            $datas = $datas->where(\DB::raw('DATE_FORMAT(r.created_at, "%m/%d/%Y")'),'>=',$startDate)->where(\DB::raw('DATE_FORMAT(r.created_at, "%m/%d/%Y")'),'<=',$endDate);
        }
        if($phoneNumber != ""){
            $datas = $datas->where('r.phone_number',$phoneNumber);
        }
        if($firstName != ""){
            $datas = $datas->where('r.first_name',$firstName);
        }
        $datas = $datas->orderByDesc('r.created_at')
            ->get();
        $datas = collect($datas)->map(function($x){ return (array) $x; })->toArray();


        $dateNow = \Carbon\Carbon::now();
        Excel::create('ข้อมูลลงทะเบียน_'.$dateNow->format('d_m_y_H_i'), function($excel) use ($datas) {
            $excel->sheet('sheet1', function($sheet) use ($datas)
            {
                $sheet->fromArray($datas);
            });
        })->download('xlsx');
    }

    public function reedeemDetailPage()
    {
        if(!array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-login');
        }


        $page = "reedeem-detail";

        return view('admin.reedeem-detail')
            ->with('page',$page);
    }

    public function customerReturnPage(Request $request)
    {
        if(!array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-login');
        }
        $page = "customer-return";

        $date = $request->date;
        $phoneNumber = $request->phone_number;
        $firstName = $request->first_name;
        $startDate = "";
        $endDate = "";
        if($date == ''){
            $startDate = Carbon::now()->format('m/d/Y');
            $endDate = Carbon::now()->format('m/d/Y');
        }else{
            $explodeDate = explode(' - ', $date);
            $startDate = $explodeDate[0];
            $endDate = $explodeDate[1];
        }

        $datas = \DB::table('dim_register as r')
            ->select(
                'r.first_name',
                'r.last_name',
                'r.year_birth',
                'r.gender',
                'r.phone_number',
                \DB::raw('count(sc.id) as total'),
                \DB::raw('DATE_FORMAT(max(sc.recieve_date), "%d/%m/%Y %H:%i:%s") as last_recieve_date')
            )
            ->leftjoin('dim_shop_customer as sc','sc.register_id','=','r.id')
            ->whereNotNull('sc.recieve_date');
        if($startDate != "" && $endDate != ""){
            $datas = $datas->where(\DB::raw('DATE_FORMAT(sc.recieve_date, "%m/%d/%Y")'),'>=',$startDate)->where(\DB::raw('DATE_FORMAT(sc.recieve_date, "%m/%d/%Y")'),'<=',$endDate);
        }
        if($phoneNumber != ""){
            $datas = $datas->where('r.phone_number',$phoneNumber);
        }
        if($firstName != ""){
            $datas = $datas->where('r.first_name',$firstName);
        }
        $datas = $datas->groupBy('r.line_user_id')->orderByDesc(\DB::raw('max(sc.recieve_date)'))
            ->paginate(10);

        return view('admin.customer-return')
            ->with('date',$date)
            ->with('phoneNumber',$phoneNumber)
            ->with('firstName',$firstName)
            ->with('datas',$datas)
            ->with('page',$page);
    }

    public function customerReturnPageExport(Request $request)
    {
        $date = $request->date;
        $phoneNumber = $request->phone_number;
        $firstName = $request->first_name;
        $startDate = "";
        $endDate = "";
        if($date == ''){
            $startDate = Carbon::now()->format('m/d/Y');
            $endDate = Carbon::now()->format('m/d/Y');
        }else{
            $explodeDate = explode(' - ', $date);
            $startDate = $explodeDate[0];
            $endDate = $explodeDate[1];
        }

        $datas = \DB::table('dim_register as r')
            ->select(
                'r.first_name as ชื่อ',
                'r.last_name as นามสกุล',
                'r.phone_number as เบอร์โทรศัพท์',
                'r.year_birth as ปีเกิด',
                'r.gender as เพศ',
                \DB::raw('count(sc.id) as "จำนวนที่ Reedeem"'),
                \DB::raw('DATE_FORMAT(max(sc.recieve_date), "%d/%m/%Y %H:%i:%s") as "วันที่ Reedeem"')
            )
            ->leftjoin('dim_shop_customer as sc','sc.register_id','=','r.id')
            ->whereNotNull('sc.recieve_date');
        if($startDate != "" && $endDate != ""){
            $datas = $datas->where(\DB::raw('DATE_FORMAT(sc.recieve_date, "%m/%d/%Y")'),'>=',$startDate)->where(\DB::raw('DATE_FORMAT(sc.recieve_date, "%m/%d/%Y")'),'<=',$endDate);
        }
        if($phoneNumber != ""){
            $datas = $datas->where('r.phone_number',$phoneNumber);
        }
        if($firstName != ""){
            $datas = $datas->where('r.first_name',$firstName);
        }
        $datas = $datas->groupBy('r.line_user_id')->orderByDesc(\DB::raw('max(sc.recieve_date)'))
            ->get();
        $datas = collect($datas)->map(function($x){ return (array) $x; })->toArray();


        $dateNow = \Carbon\Carbon::now();
        Excel::create('ผู้บริโภค ที่มีการกลับมาซื้อซ้ำ_'.$dateNow->format('d_m_y_H_i'), function($excel) use ($datas) {
            $excel->sheet('sheet1', function($sheet) use ($datas)
            {
                $sheet->fromArray($datas);
            });
        })->download('xlsx');
    }

    public function shopAccumulatedPage(Request $request)
    {
        if(!array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-login');
        }
        $page = "shop-accumulated";
        $date = $request->date;
        $outletCode = $request->outlet_code;
        $area = $request->area;
        $startDate = "";
        $endDate = "";
        if($date == ''){
            $startDate = Carbon::now()->format('m/d/Y');
            $endDate = Carbon::now()->format('m/d/Y');
        }else{
            $explodeDate = explode(' - ', $date);
            $startDate = $explodeDate[0];
            $endDate = $explodeDate[1];
        }
        $shopCustomers = \DB::table('dim_shop_customer as sc')
            ->select(
                's.shop_code',
                's.shop_name',
                's.shop_customer_name',
                's.shop_tel',
                's.shop_type',
                's.shop_area',
                'sc.shop_id',
                // \DB::Raw('(select count(*) from fact_shop_customer as fsc where sc.id = fsc.shop_customer_id) as total'),
                // \DB::Raw('sum(sc.total) as total'),
                \DB::raw('count(fsc.shop_customer_id) as total'),
                \DB::raw('DATE_FORMAT(max(sc.updated_at), "%d/%m/%Y %H:%i:%s") as last_update'),
                \DB::raw('DATE_FORMAT(max(sc.recieve_date), "%d/%m/%Y %H:%i:%s") as recieve_date')
            )
            ->leftjoin('dim_shop as s','sc.shop_id','=','s.id')
            ->leftjoin('fact_shop_customer as fsc','sc.id','=','fsc.shop_customer_id');


        if($startDate != "" && $endDate != ""){
            $shopCustomers = $shopCustomers->where(\DB::raw('DATE_FORMAT(fsc.created_at, "%m/%d/%Y")'),'>=',$startDate)->where(\DB::raw('DATE_FORMAT(fsc.created_at, "%m/%d/%Y")'),'<=',$endDate);
        }
        $shopCustomers = $shopCustomers->groupBy('s.shop_code');
        // ->havingRaw('DATE_FORMAT(max(sc.recieve_date), "%d/%m/%Y %H:%i:%s") IS NULL');

        //   	if($startDate != "" && $endDate != ""){
        // 	$shopCustomers = $shopCustomers->having(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%m/%d/%Y")'),'>=',$startDate)->having(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%m/%d/%Y")'),'<=',$endDate);
        // }
        if($area != ""){
            $shopCustomers = $shopCustomers->having('s.shop_area',$area);
        }
        if($outletCode != ""){
            $shopCustomers = $shopCustomers->having('s.shop_code',$outletCode);
        }
        $shopCustomers = $shopCustomers->orderByDesc(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%d/%m/%Y %H:%i:%s")'))
            ->paginate(10);
        // ->paginate(10);

        return view('admin.shop-accumulated')
            ->with('page',$page)
            ->with('date',$date)
            ->with('outletCode',$outletCode)
            ->with('area',$area)
            ->with('datas',$shopCustomers);
    }

    public function shopAccumulatedPageExport(Request $request)
    {
        $date = $request->date;
        $outletCode = $request->outlet_code;
        $area = $request->area;
        $startDate = "";
        $endDate = "";
        if($date == ''){
            $startDate = Carbon::now()->format('m/d/Y');
            $endDate = Carbon::now()->format('m/d/Y');
        }else{
            $explodeDate = explode(' - ', $date);
            $startDate = $explodeDate[0];
            $endDate = $explodeDate[1];
        }
        $shopCustomers = \DB::table('dim_shop_customer as sc')
            ->select(
                's.shop_name as Outlet Name',
                's.shop_customer_name as Customer Name',
                's.shop_tel as เบอร์โทรศัพท์',
                's.shop_type as Shop Type',
                's.shop_code as Outlet Code',
                's.shop_area as Area',
                \DB::raw('count(fsc.shop_customer_id) as "ยอดสะสม"'),
                \DB::raw('DATE_FORMAT(max(sc.updated_at), "%d/%m/%Y %H:%i:%s") as "ยอดสะสมล่าสุด"')
            )
            ->leftjoin('dim_shop as s','sc.shop_id','=','s.id')
            ->leftjoin('fact_shop_customer as fsc','sc.id','=','fsc.shop_customer_id');

        if($startDate != "" && $endDate != ""){
            $shopCustomers = $shopCustomers->where(\DB::raw('DATE_FORMAT(fsc.created_at, "%m/%d/%Y")'),'>=',$startDate)->where(\DB::raw('DATE_FORMAT(fsc.created_at, "%m/%d/%Y")'),'<=',$endDate);
        }
        $shopCustomers = $shopCustomers->groupBy('s.shop_code');
        // ->havingRaw('DATE_FORMAT(max(sc.recieve_date), "%d/%m/%Y %H:%i:%s") IS NULL');

        // if($startDate != "" && $endDate != ""){
        //     $shopCustomers = $shopCustomers->having(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%m/%d/%Y")'),'>=',$startDate)->having(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%m/%d/%Y")'),'<=',$endDate);
        // }
        if($outletCode != ""){
            $shopCustomers = $shopCustomers->having('s.shop_code',$outletCode);
        }
        $shopCustomers = $shopCustomers->orderByDesc(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%d/%m/%Y %H:%i:%s")'))->get();
        $datas = collect($shopCustomers)->map(function($x){ return (array) $x; })->toArray();


        $dateNow = \Carbon\Carbon::now();
        Excel::create('ร้านค้าที่มียอดสะสม_'.$dateNow->format('d_m_y_H_i'), function($excel) use ($datas) {
            $excel->sheet('sheet1', function($sheet) use ($datas)
            {
                $sheet->fromArray($datas);
            });
        })->download('xlsx');
    }

    public function customerAccumulatedPage(Request $request)
    {
        if(!array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-login');
        }
        $page = "customer-accumulated";
        $date = $request->date;
        $name = $request->name;
        $tel = $request->tel;
        $startDate = "";
        $endDate = "";
        if($date == ''){
            $startDate = Carbon::now()->format('m/d/Y');
            $endDate = Carbon::now()->format('m/d/Y');
        }else{
            $explodeDate = explode(' - ', $date);
            $startDate = $explodeDate[0];
            $endDate = $explodeDate[1];
        }
        $datas = \DB::table('dim_shop_customer as sc')
            ->select(
                'r.first_name',
                'r.last_name',
                'r.year_birth',
                'r.gender',
                'r.phone_number',
                \DB::raw('count(fsc.shop_customer_id) as total'),
                \DB::raw('DATE_FORMAT(min(fsc.updated_at), "%d/%m/%Y %H:%i:%s") as first_create')
            )
            ->leftjoin('dim_register as r','sc.register_id','=','r.id')
            ->leftjoin('fact_shop_customer as fsc','sc.id','=','fsc.shop_customer_id')
            ->whereNotNull('r.first_name');
        if($startDate != "" && $endDate != ""){
            $datas = $datas->where(\DB::raw('DATE_FORMAT(fsc.created_at, "%m/%d/%Y")'),'>=',$startDate)->where(\DB::raw('DATE_FORMAT(fsc.created_at, "%m/%d/%Y")'),'<=',$endDate);
        }
        if($name != ""){
            $datas = $datas->where('r.first_name',$name);
        }
        if($tel != ""){
            $datas = $datas->where('r.phone_number',$tel);
        }
        $datas = $datas->groupBy('r.id');
        $datas = $datas->orderByDesc(\DB::raw('DATE_FORMAT(min(fsc.updated_at), "%d/%m/%Y %H:%i:%s")'))
            ->paginate(10);
        // ->paginate(10);

        return view('admin.customer-accumulated')
            ->with('page',$page)
            ->with('date',$date)
            ->with('name',$name)
            ->with('tel',$tel)
            ->with('datas',$datas);
    }

    public function customerAccumulatedPageExport(Request $request)
    {
        $date = $request->date;
        $name = $request->name;
        $tel = $request->tel;
        $startDate = "";
        $endDate = "";
        if($date == ''){
            $startDate = Carbon::now()->format('m/d/Y');
            $endDate = Carbon::now()->format('m/d/Y');
        }else{
            $explodeDate = explode(' - ', $date);
            $startDate = $explodeDate[0];
            $endDate = $explodeDate[1];
        }
        $datas = \DB::table('dim_shop_customer as sc')
            ->select(
                'r.first_name as ชื่อ',
                'r.last_name as นามสกุล',
                'r.phone_number as เบอร์โทรศัพท์',
                'r.year_birth as ปีเกิด',
                'r.gender as เพศ',
                \DB::raw('count(fsc.shop_customer_id) as "จำนวน(ขวด)"'),
                \DB::raw('DATE_FORMAT(min(fsc.updated_at), "%d/%m/%Y %H:%i:%s") as "วันที่สะสม"')
            )
            ->leftjoin('dim_register as r','sc.register_id','=','r.id')
            ->leftjoin('fact_shop_customer as fsc','sc.id','=','fsc.shop_customer_id')
            ->whereNotNull('r.first_name');
        if($startDate != "" && $endDate != ""){
            $datas = $datas->where(\DB::raw('DATE_FORMAT(fsc.created_at, "%m/%d/%Y")'),'>=',$startDate)->where(\DB::raw('DATE_FORMAT(fsc.created_at, "%m/%d/%Y")'),'<=',$endDate);
        }
        if($name != ""){
            $datas = $datas->where('r.first_name',$name);
        }
        if($tel != ""){
            $datas = $datas->where('r.phone_number',$tel);
        }
        $datas = $datas->groupBy('r.id');
        $datas = $datas->orderByDesc(\DB::raw('DATE_FORMAT(min(fsc.updated_at), "%d/%m/%Y %H:%i:%s")'))
            ->get();
        $datas = collect($datas)->map(function($x){ return (array) $x; })->toArray();


        $dateNow = \Carbon\Carbon::now();
        Excel::create('ลูกค้าที่มียอดสะสม_'.$dateNow->format('d_m_y_H_i'), function($excel) use ($datas) {
            $excel->sheet('sheet1', function($sheet) use ($datas)
            {
                $sheet->fromArray($datas);
            });
        })->download('xlsx');
    }

    public function shopReedeemPage(Request $request)
    {
        if(!array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-login');
        }
        $page = "shop-reedeem";

        $date = $request->date;
        $outletCode = $request->outlet_code;
        $area = $request->area;
        $startDate = "";
        $endDate = "";
        if($date == ''){
            $startDate = Carbon::now()->format('m/d/Y');
            $endDate = Carbon::now()->format('m/d/Y');
        }else{
            $explodeDate = explode(' - ', $date);
            $startDate = $explodeDate[0];
            $endDate = $explodeDate[1];
        }
        $shopCustomers = \DB::table('dim_shop_customer as sc')
            ->select(
                's.shop_code',
                's.shop_name',
                's.shop_customer_name',
                's.shop_tel',
                's.shop_type',
                's.shop_area',
                'sc.shop_id',
                \DB::raw('count(sc.shop_id) as total_redeem'),
                \DB::raw('DATE_FORMAT(max(sc.updated_at), "%d/%m/%Y %H:%i:%s") as last_update'),
                \DB::raw('DATE_FORMAT(max(sc.recieve_date), "%d/%m/%Y %H:%i:%s") as recieve_date')
            )
            ->leftjoin('dim_shop as s','sc.shop_id','=','s.id');
        if($startDate != "" && $endDate != ""){
            $shopCustomers = $shopCustomers->where(\DB::raw('DATE_FORMAT(sc.recieve_date, "%m/%d/%Y")'),'>=',$startDate)->where(\DB::raw('DATE_FORMAT(sc.recieve_date, "%m/%d/%Y")'),'<=',$endDate);
        }
        $shopCustomers = $shopCustomers->groupBy('s.shop_code')
            ->havingRaw('DATE_FORMAT(max(sc.recieve_date), "%d/%m/%Y %H:%i:%s") IS NOT NULL');
        //    if($startDate != "" && $endDate != ""){
        //  $shopCustomers = $shopCustomers->having(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%m/%d/%Y")'),'>=',$startDate)->having(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%m/%d/%Y")'),'<=',$endDate);
        // }
        if($area != ""){
            $shopCustomers = $shopCustomers->having('s.shop_area',$area);
        }
        if($outletCode != ""){
            $shopCustomers = $shopCustomers->having('s.shop_code',$outletCode);
        }
        $shopCustomers = $shopCustomers->orderByDesc(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%d/%m/%Y %H:%i:%s")'))
            ->paginate(10);

        return view('admin.shop-reedeem')
            ->with('page',$page)
            ->with('date',$date)
            ->with('outletCode',$outletCode)
            ->with('area',$area)
            ->with('datas',$shopCustomers);
    }

    public function shopReedeemPageExport(Request $request)
    {
        $date = $request->date;
        $outletCode = $request->outlet_code;
        $area = $request->area;
        $startDate = "";
        $endDate = "";
        if($date == ''){
            $startDate = Carbon::now()->format('m/d/Y');
            $endDate = Carbon::now()->format('m/d/Y');
        }else{
            $explodeDate = explode(' - ', $date);
            $startDate = $explodeDate[0];
            $endDate = $explodeDate[1];
        }
        $shopCustomers = \DB::table('dim_shop_customer as sc')
            ->select(
                's.shop_code as Outlet Code',
                's.shop_name as Outlet Name',
                's.shop_customer_name as Customer Name',
                's.shop_tel as เบอร์โทรศัพท์',
                's.shop_type as Shop Type',
                's.shop_area as Area',
                \DB::raw('count(sc.shop_id) as "ยอด Redeem"'),
                \DB::raw('DATE_FORMAT(max(sc.updated_at), "%d/%m/%Y %H:%i:%s") as ยอดสะสมล่าสุด')
            )
            ->leftjoin('dim_shop as s','sc.shop_id','=','s.id');
        if($startDate != "" && $endDate != ""){
            $shopCustomers = $shopCustomers->where(\DB::raw('DATE_FORMAT(sc.recieve_date, "%m/%d/%Y")'),'>=',$startDate)->where(\DB::raw('DATE_FORMAT(sc.recieve_date, "%m/%d/%Y")'),'<=',$endDate);
        }
        $shopCustomers = $shopCustomers->groupBy('s.shop_code')
            ->havingRaw('DATE_FORMAT(max(sc.recieve_date), "%d/%m/%Y %H:%i:%s") IS NOT NULL');
        //    if($startDate != "" && $endDate != ""){
        //  $shopCustomers = $shopCustomers->having(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%m/%d/%Y")'),'>=',$startDate)->having(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%m/%d/%Y")'),'<=',$endDate);
        // }
        if($area != ""){
            $shopCustomers = $shopCustomers->having('s.shop_area',$area);
        }
        if($outletCode != ""){
            $shopCustomers = $shopCustomers->having('s.shop_code',$outletCode);
        }
        $shopCustomers = $shopCustomers->orderByDesc(\DB::raw('DATE_FORMAT(max(sc.updated_at), "%d/%m/%Y %H:%i:%s")'))
            ->get();
        $datas = collect($shopCustomers)->map(function($x){ return (array) $x; })->toArray();


        $dateNow = \Carbon\Carbon::now();
        Excel::create('ร้านค้าที่ถูก Redeem_'.$dateNow->format('d_m_y_H_i'), function($excel) use ($datas) {
            $excel->sheet('sheet1', function($sheet) use ($datas)
            {
                $sheet->fromArray($datas);
            });
        })->download('xlsx');
    }

    public function shopDetailPage(Request $request)
    {
        if(!array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-login');
        }
        $page = "shop-detail";
        $date = $request->date;
        $outletCode = $request->outlet_code;
        $nowYear = \Carbon\Carbon::now()->format('Y');
        $year = $request->year;
        if($year == ''){
            $year = \Carbon\Carbon::now()->format('Y');
        }
        $startDate = "";
        $endDate = "";
        // if($date == ''){
        //     $startDate = Carbon::now()->format('m/d/Y');
        //     $endDate = Carbon::now()->format('m/d/Y');
        // }else{
        //     $explodeDate = explode(' - ', $date);
        //     $startDate = $explodeDate[0];
        //     $endDate = $explodeDate[1];
        // }

        $datas = \DB::table('dim_shop_buyer as sb')
            ->select(
                'sb.*',
                'sb.shop_id as id',
                \DB::raw('DATE_FORMAT(sb.updated_at, "%d/%m/%Y %H:%i") as shop_buyer_updated_at')
            )
            ->where('sb.year',$year);
        // if($startDate != "" && $endDate != ""){
        //     $datas = $datas->where(\DB::raw('DATE_FORMAT(sb.updated_at, "%m/%d/%Y")'),'>=',$startDate)->where(\DB::raw('DATE_FORMAT(sb.updated_at, "%m/%d/%Y")'),'<=',$endDate);
        // }
        if($outletCode != ""){
            $datas = $datas->where('sb.shop_code',$outletCode);
        }
        $datas = $datas->orderByDesc('sb.updated_at')->paginate(10);

        return view('admin.shop-detail')
            ->with('date',$date)
            ->with('outletCode',$outletCode)
            ->with('year',$year)
            ->with('datas',$datas)
            ->with('nowYear',$nowYear)
            ->with('page',$page);
    }

    public function uploadData()
    {
        $nowYear = Carbon::now()->format('Y');
        if(!array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-login');
        }
        $page = "upload-data";
        $status = "upload-page";

        return view('admin.upload')
            ->with('nowYear',$nowYear)
            ->with('page',$page)
            ->with('status',$status);
    }

    public function uploadDataStore(Request $request)
    {
        $year = $request->year;

        $dateNow = Carbon::now()->format('dmY_His');
        $type = null;
        $destinationPath = 'file_upload/m150';
        UploadData::checkFolderDefaultPath($destinationPath);
        $fileData = $request->file_data;

        $extension = $fileData->getClientOriginalExtension(); // getting image extension
        $fileOriginalName = $fileData->getClientOriginalName();
        $fileName = $dateNow.'.'.$extension; // renameing image
        $fileData->move($destinationPath, $fileName); // uploading file to given path

        UploadData::create([
            'path_file' => "/".$destinationPath."/".$fileName,
            'file_name' => $fileOriginalName,
            'year' => $year,
            'is_active' => 1,
            'status' => 'Process'
        ]);

        return redirect('/admin-upload-thank');
    }

    public function uploadDataThank()
    {
        if(!array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-login');
        }

        $page = "upload-data";
        $status = "success-page";

        return view('admin.upload')
            ->with('status',$status)
            ->with('page',$page);
    }

    public function uploadStatusPage()
    {
        if(!array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-login');
        }
        $page = "upload-status";
        $datas = UploadData::orderByDesc('created_at')->paginate(10);

        return view('admin.upload-status')
            ->with('datas',$datas)
            ->with('page',$page);
    }

    public function adminLoginPage()
    {
        if(array_key_exists('Ji4TM6ckZDkBFDZVz0qM', $_COOKIE)){
            return redirect('/admin-home');
        }
        return view('admin.login.login');
    }

    public function adminLogout()
    {
        setcookie("Ji4TM6ckZDkBFDZVz0qM", "", time() - 3600);

        return redirect('/admin-login');
    }

    public function adminCheckLogin(Request $request)
    {
        $email = $request->email1;
        $password = $request->password;
        $user = User::where('email',$email)->first();
        if(!$user){
            return redirect()->back();
        }

        if (\Hash::check($password, $user->password))
        {
            setcookie('Ji4TM6ckZDkBFDZVz0qM', $user->id, time() + (60*60), "/");
            return redirect('/admin-home');
        }else{
            return redirect()->back();
        }
    }
}
