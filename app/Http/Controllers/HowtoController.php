<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HowtoController extends Controller
{
    public function howtoPage()
    {
    	return view('howto.index');
    }

    public function termConditionPage()
    {
    	return view('term-condition.index');
    }
}
