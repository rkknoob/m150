<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\ShopCustomer;
use App\ShopCustomerItem;
use App\RegisterData;
use App\LineUserProfile;
use Carbon\Carbon;

class CustomerCodeController extends Controller
{
    public function customerCodePage()
    {
        $id = \Session::get('register-id', '');
        \Session::put('register-id', '');
        if($id == ''){
            return redirect('/error-page');
        }
        $registerData = RegisterData::find($id);
        $lineUserProfile = LineUserProfile::find($registerData->line_user_id);
        $shopCustomers = ShopCustomer::where('register_id',$id)->where('is_active',1)->get();
        return view('customer-code.index')
            ->with('registerData',$registerData)
            ->with('registerId',$id)
            ->with('shopCustomers',$shopCustomers)
            ->with('lineUserProfile',$lineUserProfile);
    }

    public function customerCodeStore(Request $request)
    {
        $registerId = $request->register_id;
        $shopId = $request->shop_id;
        $qty = $request->qty;
        $oldTotal = 0;
        $shopCustomer = ShopCustomer::where('register_id',$registerId)->where('shop_id',$shopId)->where('is_active',1)->first();
        if($shopCustomer){
            $oldTotal = $shopCustomer->total;
            $newTotal = $oldTotal + $qty;
            $shopCustomer->update([
                'total' => $newTotal
            ]);
        }else{
            $shopCustomer = ShopCustomer::create([
                'register_id' => $registerId,
                'shop_id' => $shopId,
                'is_active' => 1,
                'total' => $qty
            ]);
        }

        for ($i=0; $i < $qty; $i++) {
            ShopCustomerItem::create([
                'shop_customer_id' => $shopCustomer->id
            ]);
        }

        return response()->json([
            'response' => 1,
        ]);
    }

    public function checkShop(Request $request)
    {
        $registerId = $request->register_id;
        $dateNow = Carbon::now()->format('Y-m-d');
        $datas = [];
        $datas['is_shop'] = 0;
        $datas['is_active_in_day'] = 1;
        $datas['customer_qty_in_day'] = 0;
        $shopCode = $request->shop_code;
        $shop = Shop::where('shop_code',$shopCode)->first();
        if($shop){
            $datas['is_shop'] = 1;
            $datas['shop_code'] = $shop->shop_code;
            $datas['shop_name'] = $shop->shop_name;
            $datas['shop_id'] = $shop->id;
            $shopCustomers =  \DB::table('dim_shop_customer as dsc')
                ->leftJoin('fact_shop_customer as fsc', 'fsc.shop_customer_id', '=', 'dsc.id')
                ->where('dsc.shop_id',$shop->id)
                ->where('dsc.register_id',$registerId)
                ->whereDate('fsc.created_at',$dateNow)
                ->get();
            $datas['customer_qty_in_day'] = $shopCustomers->count();
            if($shopCustomers->count() >= 2){
                $datas['is_active_in_day'] = 0;
            }
        }

        return response()->json([
            'response' => $datas,
        ]);
    }

    public function checkShopOnly(Request $request)
    {
        $dateNow = Carbon::now()->format('Y-m-d');
        $datas = [];
        $datas['is_shop'] = 0;
        $shopCode = $request->shop_code;
        $shop = Shop::where('shop_code',$shopCode)->first();
        if($shop){
            $datas['is_shop'] = 1;
            $datas['shop_id'] = $shop->id;
        }

        return response()->json([
            'response' => $datas,
        ]);
    }

    public function bypassCustomerCodePage($id,$shopId)
    {
        $registerData = RegisterData::find($id);
        if(!$registerData){
            return redirect('/error-page');
        }

        \Session::put('register-id', $registerData->id);
        \Session::put('shop-id', $shopId);
        return redirect('/customer-shop');
    }

}
