<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RegisterData;
use App\ShopCustomer;
use App\Shop;

class MockSetBottleController extends Controller
{
    public function mockBottlePage()
    {
    	return view('mock-bottle.index');
    }

    public function storeMockBottlePage(Request $request)
    {
    	$registerData = RegisterData::where('phone_number',$request->phone_number)->first();
    	if(!$registerData){
    		return redirect('/mock-bottle');
    	}
    	$shop = Shop::where('shop_code',$request->shop_code)->first();
    	if(!$shop){
    		return redirect('/mock-bottle');
    	}
    	$shopCustomer = ShopCustomer::where('register_id',$registerData->id)->where('shop_id',$shop->id)->where('is_active',1)->first();

    	$shopCustomer->update([
    		'total' => $request->number_bottle,
    	]);

    	return redirect('/mock-bottle');
    }
}
