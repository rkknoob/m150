<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\LineUserProfile;
use Socialite;
use Log;

class AuthController extends Controller
{
    /**
     * Redirect the user to the LINE authentication page.
     *
     * @return Response
     */
    public function redirectToProvider(Request $request)
    {
        // return Socialite::driver('line')->redirect();

        // $authUser = \App\LineUserProfile::first();

        // \Session::put('line-user-profile', $authUser);
        // return redirect('/register');

        if(!isset($_COOKIE['line-user-id'])) {
            return Socialite::driver('line')->redirect();
        } else {
            $lineUserId = $_COOKIE['line-user-id'];
            // \Log::debug('get cookie lineUserId => '.$lineUserId);
            $lineUserProfile = LineUserProfile::find($lineUserId);
            \Session::put('line-user-profile', $lineUserProfile);

            return redirect('/register');
        }
    }

    /**
     * Obtain the user information from LINE.
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        try {
            $user = Socialite::driver('line')->user();
        } catch (Exception $e) {
            return redirect()->intended('/');
        }

        $authUser = $this->findOrCreateUser($user);
        \Session::put('line-user-profile', $authUser);
        setcookie('line-user-id', $authUser->id, time() + (86400 * 1), "/"); // 86400 = 1 day
        // \Log::debug('set cookie lineUserId => '.$authUser->id);

        return redirect('/register');
        // try {
        //     $user = Socialite::driver('line')->user();
        // } catch (Exception $e) {
        //     return redirect()->intended('/');
        // }

        // $authUser = $this->findOrCreateUser($user);
        // \Session::put('line-user-profile', $authUser);
        // return redirect('/register');

        // return redirect()->action('LoaderController@firstPage');

    }

    /**
     * Logout
     *
     * @return Response
     */
    public function logout()
    {
        Auth::logout();
        return redirect()->intended('/login');
    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param object $user
     * @return User
     */
    private function findOrCreateUser($user)
    {
    	if ($authUser = \App\LineUserProfile::where('userId', $user->id)->first()) {
            return $authUser;
        }

        return \App\LineUserProfile::create([
            'userId' => $user->id,
            'name' => $user->name,
            'avatar' => $user->avatar
        ]);
    }
}
