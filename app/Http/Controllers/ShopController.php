<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\ShopCustomer;
use App\ShopCustomerItem;
use App\RegisterData;
use App\ShopBuyer;
use Carbon\Carbon;
use Excel;

class ShopController extends Controller
{
    public function shopPage()
    {
    	return view('shop.index');
    }

    public function byPassShop($shopId)
    {
    	\Session::put('shop-id', $shopId);

    	return redirect('/shop');
    }

    public function shopShowPage()
    {
    	$shopId = \Session::get('shop-id', '');
    	\Session::put('shop-id', '');
    	if($shopId == ''){
    		return redirect('/error-page');
    	}
    	$firstLastShopCustomers = ShopCustomer::where('shop_id',$shopId)->get();
        $firstLastShopBuyers = ShopBuyer::where('shop_id',$shopId)->get();
        $startDateEndDate = "-";
    	$startDateEndDateShopBuyer = "-";
        $shopBuyerSum = 0;
    	if($firstLastShopCustomers->count() > 0){
    		$firstDate = clone $firstLastShopCustomers;
    		$firstDate = $firstDate->first()->created_at->format('d-m-Y');
    		$lastDate = clone $firstLastShopCustomers;
    		$lastDate = $lastDate->last()->created_at->format('d-m-Y');
    		$startDateEndDate = $firstDate." - ".$lastDate;
    	}
        if($firstLastShopBuyers->count() > 0){
            $firstDate = clone $firstLastShopBuyers;
            $firstDate = $firstDate->first()->created_at->format('d-m-Y');
            $lastDate = clone $firstLastShopBuyers;
            $lastDate = $lastDate->last()->created_at->format('d-m-Y');
            $startDateEndDateShopBuyer = $firstDate." - ".$lastDate;
            $shopBuyerSum = $firstLastShopBuyers->sum('quanlity');
        }
    	$shop = Shop::find($shopId);
    	$countRecieve = ShopCustomer::where('shop_id',$shopId)->where('is_recieve',1)->count();
    	$countAll = ShopCustomer::where('shop_id',$shopId)->get()->sum('total');

    	return view('shop-show.index')
    		->with('shop',$shop)
    		->with('countRecieve',$countRecieve)
    		->with('countAll',$countAll)
            ->with('startDateEndDate',$startDateEndDate)
            ->with('startDateEndDateShopBuyer',$startDateEndDateShopBuyer)
    		->with('shopBuyerSum',$shopBuyerSum);
    }

    public function byPassShopCustomerDetail($shopId)
    {
        return redirect('/shop-customer-detail/'.$shopId);
    }

    public function shopCustomerDetailPage($id)
    {
        // $shopId = \Session::get('shop-id', '');
        // \Session::put('shop-id', '');
        // if($shopId == ''){
        //     abort(404);
        // }
        $shopId = $id;
        $shopCustomers = \DB::table('dim_shop_customer as sc')
            ->leftJoin('dim_register as r', 'sc.register_id', '=', 'r.id')
            ->select('r.first_name','r.last_name',\DB::raw('SUM(sc.total) as total_sum'))
            ->where('sc.shop_id',$shopId)
            ->orderByDesc('total_sum')
            ->groupBy('sc.register_id','r.first_name','r.last_name')
            ->paginate(10);

        return view('shop-show-detail-customer.index')
            ->with('shopCustomers',$shopCustomers);
    }

    public function importFile(Request $request)
    {
        $results = Excel::load($request->file_data, function($reader) {

        })->all()->toArray();

        foreach ($results as $key => $result) {
            $shop = Shop::where('shop_code',$result['outlet_code'])->first();
            if($shop){
                $shop->update([
                    'shop_area' => $result['shop_area']
                ]);
            }
        }

        // foreach ($results as $key => $result) {
        //     Shop::create($result);
        // }
    }
}
