<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RegisterData;

class RegisterController extends Controller
{
    public function indexPage()
    {
        $lineUserProfile = \Session::get('line-user-profile', '');
        \Session::put('line-user-profile', '');
        if($lineUserProfile == ''){
            return redirect('/error-page');
        }

        $registerData = RegisterData::where('line_user_id',$lineUserProfile->id)->first();
        if($registerData){
            return redirect('/by-pass/'.$registerData->id);
        }

        return view('register.index')
            ->with('lineUserProfile',$lineUserProfile);
    }

    public function storeData(Request $request)
    {
        // $phoneNumber = "";
        // $birthYear = "";
        //    foreach ($request->year as $key => $year) {
        //        $birthYear .= $year;
        //    }

        //    foreach ($request->tel as $key => $tel) {
        //        $phoneNumber .= $tel;
        //    }
        $registerData = RegisterData::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'year_birth' => $request['year_birth'],
            'gender' => $request['gender'],
            'phone_number' => $request['phone_number'],
            'line_user_id' => $request['line_user_id'],
        ]);
        \Session::put('register-id', $registerData->id);
        return redirect('/customer-code');
        // \Session::put('register-id', $registerData->id);
    }

    public function checkImei(Request $request)
    {
        $check = 0;
        $regisDataId = 0;
        $imei = $request->imei;
        // dd($uuid);
        $registerData = RegisterData::where('imei',$imei)->first();
        if($registerData){
            $check = 1;
            $regisDataId = $registerData->id;
        }

        return response()->json([
            'is_check'   => $check,
            'regis_data_id'   => $regisDataId,
        ]);
    }

    public function bypassRegisterPage($id)
    {
        $registerData = RegisterData::find($id);

        \Session::put('register-id', $registerData->id);
        return redirect('/customer-code');
    }
}
