<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UploadData\UploadData;
use App\ShopBuyer;
use App\Shop;
use Excel;

class JobScheduleFunction extends Model
{
    public static function runImportFile()
    {
    	$datas = UploadData::where('is_active',1)->where('status','Process')->get();
    	foreach ($datas as $key => $data) {
    		$data->update([
    			'is_active' => 0
    		]);

    		$year = $data->year;
    		$file = public_path()."/"."/file_upload/m150/27092018_013250.xlsx";
	        $results = Excel::load($file, function($reader) {

	        })->all()->toArray();
	        foreach ($results as $key => $result) {
	            $result['year'] = $year;
	            $result['shop_code'] = $result['new_outlet_code'];
	            $shop = Shop::where('shop_code',$result['shop_code'])->first();
	            if($shop){
	            	$result['shop_id'] = $shop->id;
	            	$result['shop_name'] = $shop->shop_name;
	            	$result['shop_customer_name'] = $shop->shop_customer_name;
	            	$result['shop_tel'] = $shop->shop_tel;
	            	$result['shop_type'] = $shop->shop_type;
	            	$result['shop_area'] = $shop->shop_area;
	            }
	            $shopBuyer = ShopBuyer::where('shop_code',$result['shop_code'])->where('year',$result['year'])->first();
	            if($shopBuyer){
	                $shopBuyer->update($result);
	            }else{
	                ShopBuyer::create($result);
	            }
	        }

	        $data->update([
    			'status' => 'Success'
    		]);
    	}
    }
}
