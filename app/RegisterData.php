<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterData extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_register';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
		'last_name',
		'year_birth',
		'gender',
		'phone_number',
		'line_user_id',
    ];
}
