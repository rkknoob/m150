<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Shop;

class ShopCustomer extends Model
{
    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dim_shop_customer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'register_id',
		'shop_id',
		'is_active',
		'is_recieve',
		'recieve_date',
		'total',
    ];

    public function shop()
    {
        return $this->belongsto(Shop::class,'shop_id','id');
    }
}
