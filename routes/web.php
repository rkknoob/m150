<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

// Route::get('/admin-page', function (Request $request) {
// 	return view('admin.home');
// });

Route::get('/re-gen-customer-buy', function (Request $request) {
	$shopBuyers = \App\ShopBuyer::all();
	foreach ($shopBuyers as $key => $shopBuyer) {
		$shop = \App\Shop::where('shop_code',$shopBuyer->shop_code)->first();
		if($shop){
			$shopBuyer->update([
				'shop_id' => $shop->id,
				'shop_name' => $shop->shop_name,
				'shop_customer_name' => $shop->shop_customer_name,
				'shop_tel' => $shop->shop_tel,
				'shop_type' => $shop->shop_type,
				'shop_area' => $shop->shop_area
			]);
		}
	}
});

Route::get('/gen-user', function (Request $request) {
	\App\LoginData\User::create([
		'email' => "admin@m150.yellow-idea.com",
		'password' => bcrypt('88888888')
	]);
});

Route::get('/select-page', function (Request $request) {
	$page = $request->page;
    return view($page);
});

Route::get('/', function () {
	return redirect()->action('Auth\AuthController@redirectToProvider');
	// return redirect('/register');
    // return view('welcome');
});

Route::get('/error-page', function () {
    return view('custom-error.index');
});

Auth::routes();

Route::get('clear-data', function (Request $request) {
	\App\RegisterData::truncate();
	\App\ShopCustomer::truncate();
	\App\ShopCustomerItem::truncate();
});

Route::get('/shop-import', function () {
    return view('import-shop.index');
});

Route::get('m150', 'Auth\AuthController@redirectToProvider');
Route::get('callback', 'Auth\AuthController@handleProviderCallback');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register', 'RegisterController@indexPage');
Route::post('/register', 'RegisterController@storeData');
Route::get('/check-imei', 'RegisterController@checkImei');
Route::get('by-pass/{id}', 'RegisterController@bypassRegisterPage');
Route::get('/customer-code', 'CustomerCodeController@customerCodePage'); //หน้าใส่ชื่อร้าน
Route::get('/customer-store-code', 'CustomerCodeController@customerCodeStore');
Route::get('/check-shop', 'CustomerCodeController@checkShop');
Route::get('/check-shop-only', 'CustomerCodeController@checkShopOnly');
Route::get('by-pass-customer-code/{id}/{shopId}', 'CustomerCodeController@bypassCustomerCodePage');
Route::get('/customer-shop', 'CustomerShopController@customerShopPage'); //แสดง จำนวนที่ลูกค้าซื้อ
Route::get('/by-pass-waiting-re/{id}/{shopId}', 'CustomerShopController@byPassWaitingRecieve');
Route::get('/customer-shop-recieve-wait', 'CustomerShopController@waitingReceivePage');
Route::get('/customer-shop-recieve', 'CustomerShopController@customerShopRecieve');
Route::get('/by-pass-recieve/{id}/{shopId}', 'CustomerShopController@byPassRecieve');
Route::get('/customer-recieve', 'CustomerShopController@customerRecievePage');
Route::post('/shop-import', 'ShopController@importFile');
// Route::get('/shop-check', 'ShopController@shopPage');
// Route::get('/by-pass-shop/{shopId}', 'ShopController@byPassShop');
// Route::get('/shop', 'ShopController@shopShowPage');
// Route::get('/by-pass-shop-cus-de/{shopId}', 'ShopController@byPassShopCustomerDetail');
// Route::get('/shop-customer-detail/{shopId}', 'ShopController@shopCustomerDetailPage');
Route::get('/howto', 'HowtoController@howtoPage');
Route::get('/term-condition', 'HowtoController@termConditionPage');

Route::get('/mock-bottle', 'MockSetBottleController@mockBottlePage');
Route::post('/mock-bottle', 'MockSetBottleController@storeMockBottlePage');


/////////////////////////////////////////Admin Page//////////////////////////////////////////////////
Route::get('/admin-home', 'AdminController@homePage');
Route::get('/admin-register-detail', 'AdminController@registerDetailPage');
Route::get('/admin-register-detail-export', 'AdminController@registerDetailPageExport');
Route::get('/admin-reedeem-detail', 'AdminController@reedeemDetailPage');
Route::get('/admin-customer-return', 'AdminController@customerReturnPage');
Route::get('/admin-customer-return-export', 'AdminController@customerReturnPageExport');
Route::get('/admin-shop-accumulated', 'AdminController@shopAccumulatedPage');
Route::get('/admin-shop-accumulated-export', 'AdminController@shopAccumulatedPageExport');
Route::get('/admin-customer-accumulated', 'AdminController@customerAccumulatedPage');
Route::get('/admin-customer-accumulated-export', 'AdminController@customerAccumulatedPageExport');
Route::get('/admin-shop-reedeem', 'AdminController@shopReedeemPage');
Route::get('/admin-shop-reedeem-export', 'AdminController@shopReedeemPageExport');
Route::get('/admin-shop-detail', 'AdminController@shopDetailPage');
Route::get('/admin-upload', 'AdminController@uploadData');
Route::post('/admin-upload-store', 'AdminController@uploadDataStore');
Route::get('/admin-upload-thank', 'AdminController@uploadDataThank');
Route::get('/admin-upload-status', 'AdminController@uploadStatusPage');

Route::get('/admin-login', 'AdminController@adminLoginPage');
Route::get('/admin-logout', 'AdminController@adminLogout');
Route::post('/admin-check-login', 'AdminController@adminCheckLogin');
/////////////////////////////////////////////////////////////////////////////////////////////////////
